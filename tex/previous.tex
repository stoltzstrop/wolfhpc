\section{Previous Work}
\label{sec:prevwork}

\par
Previous research has shown that it is possible to develop more productive and performance portable codes for simulation codes such as room acoustics\cite{reesedafx}.
A study was done across various architectures with a number of different implementations of the same room acoustics benchmark using different programming models to determine limiting factors and behavior.
Memory bandwidth was shown to be the limiting factor for this benchmark as can be seen that it runs fastest on the platform with the highest memory bandwidth (AMD R9 295X2).
In addition, a more productive, bespoke framework was developed to test out different data layouts and optimizations. 

\subsection{Performance Comparison of Software}
\label{sec:perfcomp}
\par
Four implementations of the same room acoustics benchmark (CUDA, OpenCL, abstractCL and targetDP) were run on six platforms (Intel Xeon CPU, Xeon Phi Knights Corner, NVIDIA K20, NVIDIA GTX 780, AMD R280 and AMD R9 259X2). 
Obviously not all versions were able to run on all platforms - CUDA and targetDP which uses CUDA on GPUs - are limited to NVIDIA platform GPUs. 
All other versions utilize OpenCL which can run on all selected platforms.
\textit{targetDP} is a low-level library for running simulation codes on NVIDIA GPUs, CPUs or Xeon Phis without rewriting the code\cite{targetDP}.
\textit{abstractCL} is a framework tied to room acoustics simulations that we built using C++ macros and OpenCL to allow simplified swapping in and out of data layouts and optimizations\cite{reesedafx}. 
More about \textit{abstractCL} can be found in Section \ref{sec:absCL}.
Specifications for the various platforms can be seen in Table \ref{tab:specsmatrix}. 
Performance was generally comparable on all GPU platforms and more widely pronounced on the Xeon Phi and CPU (though getting reproducible results on the Xeon Phi proved to be tricky).
However performance across platforms differed tremendously, up to 40-50\% between GPUs as can be seen in Figure \ref{fig:allTimings}.

\begin{table}[H]
\centering
    \resizebox{1.0\hsize}{!}{\begin{tabular}{|c|c|c|c|c|}
        \hline
          \textbf{Platform} &  \textbf{Number of Cores/} & \textbf{Peak Bandwidth (GB/s)} & \textbf{Peak GFlops} & \textbf{Memory (MB)}\\ 
           &  \textbf{Stream Processor} &  & (Double Precision) & \\ \hline \hline
          NVIDIA K20 & 2496 & 208 & 1175 & 5120 \\ \hline
          NVIDIA GTX 780 & 2304  & 288.4 & 165.7 & 3072 \\ \hline
          AMD R280     & 2048  & 288  & 870  & 3072 \\ \hline
          AMD R9 259X2 & 2816  & 320 & 716.67  & 4096 \\ \hline
          Xeon Phi 5110P    & 60  & 320 & 1011  & 8000 \\ \hline
          Intel Xeon E5-2670   & 32  & 51.2 & 166.4 & 126000 \\ \hline
    \end{tabular}}
    \caption{Specification Table. This table shows the specifications of the various platforms used in previous work.} 
    \label{tab:specsmatrix}
\end{table}

\begin{figure}
  \centering
 \resizebox{1\hsize}{!}{\includegraphics[width=17cm]{otherpapers/imagesdafx/allTimingsComparison512.pdf}}
  \caption{Performance timings for simple room acoustics benchmark implementations across various platforms using room sizes of 512x512x404 grid points.}
  \label{fig:allTimings}
\end{figure}


\subsubsection{abstractCL}
\label{sec:absCL}
\textit{abstractCL} was created to make room simulation kernels on-the-fly, depending on the type of run a user wants to do. 
The type of variations can be between different data layouts of the grid passed in to represent the room, hardware-specific optimizations or both.
This is done through swapping in and out relevant files that include overloaded functions and definitions in the main algorithm itself. 
The data abstractions and optimizations  investigated for this project include: thread configuration settings, memory layouts and memory optimizations. 
Algorithmic changes can be introduced by adding new classes to the current template for more complicated codes.
\par
abstractCL works through the use of flags which determine what version should be run. 
Certain functions must always be defined as dictated by a parent  class.
However, those functions' implementations can be pulled in from different sources and concatenated together to create the simulation kernel before compilation. 
This framework runs similarly to the other benchmark versions, apart from that the kernel is created before the code is run (which creates more initial overhead).  
It was developed in C++ (due its built-in functionality for classes, templates, inheritance and strings) as well as OpenCL.
As shown in Figure \ref{fig:allTimings}, the framework produces code that is on par with hand-tuned openCL codes.

\subsection{Data Abstraction Comparison}
\par
\textit{abstractCL} was developed to allow for different data layouts and optimizations to be tested out quickly and easily. 
Several data layouts were tested out showing a range of performance variation across platforms. 
Figure \ref{fig:DataStructures} shows the results of this experiment. 
This test showed how careful one must be about abstractions, even at the low-level. 
It is difficult to program high-level abstractions with good performance without some kind of code generation. 
\begin{figure}[H]
  \center
 \resizebox{1.1\hsize}{!}{\includegraphics[width=17cm]{otherpapers/imagesmasters/structComparisonKernel.pdf}}
  \caption{Comparison of performance of various data structures across different platforms for a room size of 256x265x202. 
  %A guide to the different layout versions can be found in Table \ref{tab:datalayout}.
  }
  \label{fig:DataStructures}
\end{figure}



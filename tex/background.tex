
\section{Background}
\label{sec:background}

Two of the main topics in this paper include room acoustics simulations and the \textsc{lift} language.
While this end goal of this project is to provide solutions to 3D wave models generally, previous and current work has predominantly focused on room acoustics simulations. 
This work has focused on analysis more than development, however current work is now using the \textsc{lift} language as discussed later in Section \ref{sec:modelinlift}.
This language is being developed as an intermediary representation of parallel algorithms. 
Both are described in more detail below. 

\subsection{Room Acoustics and Other 3D Wave Models}
Room acoustics simulations were developed to model sound waves in an enclosed three dimensional space.
The simulations use first physical principals to represent the properties of  sound as it moves through space and time.
These ``room codes'' use grids as data types that update values via the very a commonly used nearest-neighbors technique.  
The output of these simulations can provide composers or architects the ability to hear what a composition or noise would sound like in a space without actually being there or even having built it.
\par

3D wave-based simulations are an important tool in physics for modeling the evolution of waves through space and time of various mediums. 
The finite difference time domain method (FDTD) is a widely used numerical approach for modeling of the 3D wave equation\cite{botteldooren}, which is shown in Equation \ref{fig:3Dwave}.

\begin{equation}
\frac{\partial^2 \Psi}{\partial t^2} = c^2\nabla^2 \Psi
\label{fig:3Dwave}
\end{equation}

Space is discritised into a three-dimensional grid of points, with data values resident at each point representing the acoustic field at that point.
The state of the system evolves through time-stepping: values at each point are repeatedly updated using ``finite differences'' of values in the neighborhood of that point.
This algorithm (also known as a stencil) updates points as determined by the choice of discretisation scheme for the partial differential operators in the wave equation \cite{bilbao2013large}.  
This approach can be computationally expensive, however parallelization techniques have shown great improvements in performance.
Though there has been progress in the development of techniques to exploit modern parallel hardware, much of it is low-level or tied to specific platforms. 
Ideally, scientific models should be able to run in a portable manner across different architectures while retaining performance and being straightforward to program.

\subsection{The Lift Language}

%Advantages: Functional, reusable, rewrite rules. High level primitives -> low-level optimized code.

The \textsc{lift} language was developed as a modular solution to the performance portability problem\cite{liftcgo}. 
The idea is that by separating HPC programming into separate levels of abstraction, then each level can be restricted to its particular purpose. 
The language introduces three key concepts: functional, reusable high-level primitives; rewrite rules describing exchangeable relationships between primitives and a code generating search space which determines the best version of a kernel to run.
At the higher level, the language is composed of ``reusable primitives,'' written in a functional style. 
Algorithms for a wide variety of applications (including stencils) can be rewritten as compositions of these primitives. 
Figure \ref{fig:primitives} shows a sampling of some of these primitives and how they work.
``Rewrite rules'' describe a variety of transformations of a given algorithm decomposition, which are provable to return the same result.
A search space of these rewrite rules is automatically explored to optimize codes for a particular platform.
Kernels are then generated in OpenCL for all the different ``rewrites'' until an optimal program is found for a particular platform. 
Currently, the only backend \textsc{lift} supports is OpenCL, however the modular design of the language makes it easily extensible to other parallel programming frameworks.

\begin{figure}
  \centering
 \resizebox{1\hsize}{!}{\includegraphics[width=17cm]{algorithmicprimitivescut.pdf}}
 \caption{Algorithmic Primitives in the \textsc{Lift} Language}
  \label{fig:primitives}
\end{figure}



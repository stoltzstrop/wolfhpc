\section{A Modular Approach Using the \textsc{Lift} Language}
\label{sec:liftlanguage}

In the next section, we discuss our modular approach aimed at achieving productivity, performance and portability automatically using the \textsc{lift} language. 
However, \textsc{lift} is more than just a language: it is also a framework.
The language provides the algorithmic primitives to compose algorithms in and the framework handles the language compilation, rewrite rule search space and low-level code optimization.
Its goal is to target a multitude of application domains, not just stencils, so the bespoke language used in the framework is rich in functionality. 
To better understand this process, we step through a simplified example of a room acoustic simulation written in \textsc{lift}.

\subsection{Our Vision}
\par Instead of writing yet another stencil library or DSL to address the shortcomings of current solutions, this project aims to take advantage of what is available already and create a modularized approach to HPC 3D wave model development using existing DSLs and the \textsc{Lift} framework.
In an ideal world, DSLs would use a common compiler, so that the writers of these languages could focus on the needs of their specific domain in the abstraction layer instead of also having to manage hardware-specific low-level optimizations.
This is where the \textsc{lift} language provides great leverage.
\textsc{lift} is designed to be used as an intermediate layer targeted by DSLs to handle the low-level implementation details and relevant optimizations.
Unlike many existing approaches, the \textsc{lift} language and its corresponding compiler are designed to address the performance portability challenge as a first priority. 
Because of its modular design using a suite of composable algorithmic primitives, \textsc{lift} can be used to target a host of different applications.  
This also means that \textsc{lift} is hardware-agnostic and can readily be adopted to future platforms.


\subsection{Lift Overview}
The \textsc{lift} language has been developed in Scala as a modular solution to the performance portability problem\cite{lift,liftcgo}. 
The idea is that by separating HPC programming into separate levels of abstraction, then each level can be restricted to its particular purpose. 
An overview of this process can be seen in Figure \ref{fig:overview}.
At the top level (``high level abstractions'') a DSL would compile into the \textsc{lift} language which is comprised of a suite of algorithmic primitives (such as \textit{map}, \textit{reduce}, \textit{zip}, etc).
This may require a version of an API to be developed in \textsc{lift} to connect these layers more fluidly.
Different versions of an algorithm composed using these primitives (as well as more low level language specific ones) can then be explored using rewrite rules, which explore different optimization options. 
These versions can then be run on different platforms to determine the best-performing version for a different platform and this is done by generating OpenCL kernels from the different rewrite rule versions.
The only backend that \textsc{lift} currently supports is OpenCL, which provides portability to a number of different hardware platforms.

\par
The \textsc{lift} language provides three key functionalities: (1) functional, reusable high-level primitives (2) rewrite rules describing exchangeable relationships between compositions of primitives and (3) a code generating search space which determines the best version of a kernel to run on a particular platform.
At the higher level, the language is composed of ``reusable primitives,'' written in a functional style. 
Algorithms for a wide variety of applications (including stencils) can be rewritten as compositions of these primitives. 
``Rewrite rules'' describe a variety of transformations of a given algorithm decomposition, which are formalized and proven not to change a program's semantics.
While the only backend \textsc{lift} supports so far is OpenCL, the modular design of the language makes it easily extensible to other parallel programming frameworks.
One limitation of \textsc{lift} is that it is not productive on its own, however we see this as a feature meaning the language can focus on producing portable and performant code for a wide range of applications without having to provide a high-level interface to program them in.

\begin{figure}
  \centering
  \resizebox{.75\hsize}{!}{\includegraphics[width=17cm]{otherpapers/images/liftOverview.pdf}}
 \caption{Overview of the \textsc{Lift} Framework}
  \label{fig:overview}
\end{figure}

\subsection{Working Example: Room Acoustics Simulation in Lift}
\label{sec:acoustics}
In order to better explain how \textsc{lift} works, we step through a simplified example of a room acoustics simulation benchmark.
This algorithm can be seen in Listing \ref{lst:complexStencil}.
This simulation was developed by HPC physicists~\cite{craigthesis} and models the behavior of a sound wave as it propagates from a source to a receiver in an enclosed 3-dimensional space  (the updates at the receiver end are done in another less computationally intensive kernel not shown here).
Where the sound encounters a physical boundary (in this example, the walls are only the boundaries of the grid), the coefficients used in calculating the physical properties of the sound wave are adjusted according to the reflection.
Sound waves are  discritised in space as well as time, but only spatial discretisation is parallelizable, which is shown in this example.

\par{\textbf{Multiple Grid Inputs}}
The two inputs used in this benchmark (\textit{$grid_{t-1}$} and \textit{$grid_t$} on lines~\ref{lst:complexStencil:inp1}-~\ref{lst:complexStencil:inp2}) are the same size and indicate previous and current time steps in order to update the state of the room across time (and which get swapped on each iteration).
This type of inputs is often found in real world physical simulations, which span three dimensions for physical space and one for time.
These grids are zipped together as one input to the algorithm along with an on-the-fly array which calculates the number of neighbors for a given point based on a provided function (\texttt{computeNumNeighbors}).
This last grid serves as a mask for the boundaries.
The first grid is taken point-by-point, however the second grid uses \textit{slide3D} to calculate 27-point neighborhoods around the point of interest in order to retain neighboring points for the stencil.
In this 3D case, \textit{slide} returns a cube of values each one point away from the original value resulting in the 27 points.
The number of neighborhoods correctly matches up to the size of the $grid_t$ input array as the $grid_{t-1}$ input is padded using the \textit{pad3D} primitive first so that no out of bounds accesses occur.
These inputs are then zipped together with their number of neighbors (same for each grid) resulting in a tuple of: \textsc{\{value$_{t-1}$, neighborhood$_t$, numNeighbors\}} as seen on lines~\ref{lst:complexStencil:zip1}-~\ref{lst:complexStencil:getnumneighbors}.
The resulting output of the next timestep of the room state is calculated using parts of all three elements of this tuple.


\par{\textbf{Calculating the Stencil}}
For the neighborhood part of the amalgamated input, the \textit{at} primitive (expressed with []) makes it easy to calculate a more complicated stencil as can be seen on lines~\ref{lst:complexStencil:stencil1}-~\ref{lst:complexStencil:stencil2}.
The result can then be combined with the other inputs in an equation to model the sound (lines ~\ref{lst:complexStencil:equationStart}--\ref{lst:complexStencil:equationEnd}).
In this case, 27 points are passed in for the neighborhood of points one value away in any 3D direction and any of these points could be pulled out in any shape.
In this instance, only 7 points are used for the equation to calculate the stencil: values to the left, right, up, down, top and bottom.
%However, leggy stencils could be calculated using larger neighborhoods.


\par{\textbf{Boundary Handling}}
One of the most difficult problems for wave-based simulations is accurate physical boundary handling, which can involve the use of states to retain memory.
This simplified version uses state-free boundary conditions, which involve variable coefficients, however the same ideas could be applied to more complicated state conditions.
The variable coefficients (also known as ``loss'' at the boundary, in the physical sense) are handled through the use of a mask, which returns a different value depending on whether it is on a boundary or not.
The mask is calculated on the fly as an input using the \textit{boundaryGrid} generator function and contains a value at each point in the grid of the number of available neighbors for a point (ranging from 3 at a corner to 6 on the insides).
The coefficients are then calculated using the \textit{getCF} function as can be seen on line~\ref{lst:complexStencil:equationStart}.
For those values which are on the border (i.e., \textit{numNeighbors} < 6), a lossy coefficient is used in the equation (\textit{CSTloss1} or \textit{CSTloss2}).
The overall computation is based on a discretized version of the 3D wave equation to simulate the energy at different points in the room.
%\newpage
\begin{lstlisting}[caption=Room acoustic simulation as expressed in the LIFT  language,label=lst:complexStencil,float,mathescape=true]
acousticStencil(grid$_{t-1}$:[[[float]$_{m}$]$_{n}$]$_{o}$,$\label{lst:complexStencil:inp1}$
                grid$_t$:[[[float]$_{m}$]$_{n}$]$_{o}$) {  $\label{lst:complexStencil:inp2}$
  map3D(m -> {
   val valueGrid$_t$ = m.0
   val sumGrid$_{t-1}$ =$\label{lst:complexStencil:stencil1}$
    m.1[0][1][1] + m.1[1][0][1] + m.1[1][1][0] +
    m.1[1][1][2] + m.1[1][2][1] + m.1[2][1][1]$\label{lst:complexStencil:stencil2}$
    val numNeighbor = m.2 $\label{lst:complexStencil:numneighbor}$
    return getCF(m.2, CSTloss1, 1.0f) * ((2.0f -$\label{lst:complexStencil:equationStart}$
				CST$_{l2}$ * numNeighbor) * m.1[1][1][1] +
				CST$_{l2}$ * sumGrid$_{t-1}$ - getCF(m.2,
				CSTloss2, 1.0f) * valueGrid$_t$)  $\label{lst:complexStencil:equationEnd}$
  },
  zip3D(grid$_t$,$\label{lst:complexStencil:zip1}$
        slide3D(3, 1,
                pad3D(1,1,1,zero,grid$_{t-1}$)),
        boundaryGrid(m,n,o,computeNumNeighbors)))  $\label{lst:complexStencil:getnumneighbors}$
}
\end{lstlisting}

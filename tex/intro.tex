\section{Introduction}
% importance of simulations generally
\par
Computer simulations are a critically important tool in scientific fields that bridge theory with reality. 
Many of these simulations - such as 3D wave-based models like room acoustics\cite{craigthesis} or ground penetrating radar\cite{gprmax} -  are complicated to model and even more difficult to abstract about.
The difficulty in writing abstractions for these algorithms stems from absorbing boundary conditions, multiple timesteps and varying sized stencils used in the models. 
However, such simulations are integral to predicting the properties and behavior of the physical world around us. 
As such, the ability to program these simulations in a productive way which can perform well across the increasing range of HPC architectures is of growing importance.

% why simulations need all three of perf port prod and why they can't get them
\par
Computing systems have moved towards parallel (as well as heterogeneous) architectures as greater performance can no longer be achieved through a single core.
The types of architectures available are changing and increasing in numbers to meet demands for performance for scientific codes which have traditionally been run on CPUs.
It is therefore crucial to accommodate portability across both traditional and emerging platforms in order to avoid having to rewrite codes as new platforms emerge. 
Currently, many scientific groups utilize multiple code bases, which is error-prone and time-consuming to maintain.
Computational scientists should be able to focus on their own research and not require HPC expertise for re-tuning and rewriting when newer, more performant platforms emerge.
Furthermore, even where codes can be ported from one architecture to another, there is often no guarantee they will retain the same performance level on the new platform.

\par 
This paper proposes a novel, modular approach to tackling these issues of performance, portability and productivity for wave simulation models by adapting and connecting existing high-level frameworks.
Our application area of interest is 3D wave models with absorbing boundary conditions.
Our approach is two-fold: extend an existing stencil DSL with a high level of productivity to compile into the \textsc{lift} language and then enable the \textsc{lift} language to accommodate more complex stencils (for 3D wave models) to create performance portable, hardware-agnostic code.
The \textsc{lift} language acts as an ``assembly language of HPC,'' which can be used for a variety of applications including stencils. 
It is a pattern-based language and compiler addressing the performance portability problem through automatic exploration of optimizations of rewrite rules\cite{lift}.
We propose to further develop this language to enable computation and optimization for stencils from 3D wave models.
We will then extend a productive top-level DSL to compile into \textsc{Lift}, which can then use \textsc{Lift}'s composability and rewrite rules to compile simulations down into performant and  portable code.
This will create a modular workflow that will enable performance, portability and productivity for 3D wave models which could serve as a template for other types of algorithms.

 %breakdown of paper 
%% Motivation :
\par

A large number of high-level parallel methodologies currently exist which focus on stencil applications, however none provide all three of performance, portability and productivity.
The strengths and weaknesses of these approaches  will be discussed further in Section \ref{sec:relatedwork}.
Then, some background about 3D wave models and previous related work is discussed in Section \ref{sec:wavemodels}.
The results in this section provide baselines (as well as motivation) for our current work with \textsc{lift}.
Next, the \textsc{lift} intermediary language which addresses the performance portability challenge is introduced in Section \ref{sec:liftlanguage}. 
Current and ongoing work is discussed at length in Section \ref{sec:modelinlift}.
Future work involving the \textsc{lift} framework is covered in Section \ref{sec:future}.
Finally, the paper concludes in Section \ref{sec:conclusion}.


\section{3D Wave Model Development Using the \textsc{Lift} Framework}
\label{sec:modelinlift}
\par
Our current work focuses on enhancing \textsc{lift} to accommodate room acoustics stencils. 
These codes now run with comparable results to the original hand-written versions (those shown in Section \ref{sec:wavemodels}).
Several additions to the language have been made to obtain these results.
Performance still lags behind optimized versions, however, so 2.5D-tiling (which was used in versions of the original benchmark) and other optimizations are being investigated as additions to the language as well. 

\subsection{Updating Lift to Accommodate Complex 3D Stencils}
\label{sec:liftopt}
Preliminary results have shown that \textsc{Lift} is capable of expressing stencils of varying types and sizes\cite{bastianthesis}.
In particular, simplified room acoustics simulations have been thoroughly investigated in the framework and a number of other 2D and 3D stencil benchmarks have also been implemented. 
Additionally, ground penetrating radar algorithms are also being implemented in the language as they have similar wave behavior to room acoustics. 
Though both simulations are modeled using the 3D wave equation and have absorbing boundary conditions, GPR codes require modeling both the electric and magnetic fields interacting with each other.

\par
Performance values for various versions of room acoustics implementations can be seen in Figure \ref{fig:liftCompare}.
The two bars furthest to the right on each of the platforms show the original room acoustics codes written in C and OpenCL. 
The black line across the graph indicates the level of the unoptimized original benchmark.
The three bars on the left are versions of the \textsc{lift} implementations with optimization additions.
As can be seen, these have dramatically improved the implementation, but have not yet completely closed the performance gap of the optimized version of the original code.

\begin{figure}[H]
\flushleft
 \resizebox{1\hsize}{!}{\includegraphics[width=17cm]{otherpapers/images/liftVersionsComparison512.pdf}}
 \caption{Comparison of optimization versions in the \textsc{lift} framework implementation of room acoustic benchmark with grid size of  512x512x404 grid points.}
  \label{fig:liftCompare}
\end{figure}


\par
As shown, implementation of the room acoustic stencils involved two stages of optimizations.
These were adding a \textit{select} and a \textit{boundary} optimization.
Figure \ref{fig:optimizations} shows a visual representation of this as well as how the code differs in C versus \textsc{lift}.
At the top left, it can be seen that the neighborhoods produced are pulled out into the shape of interest - in this case a 5-point 2D stencil - using \textit{select}.
Previously, all nine values in memory were being accessed as that is what the neighborhood collected by \textit{slide} returns.
This is shown by the blue square blotted out by a green cross, showing that only the green cross values are now being accessed.
Additionally, masks of the same size as input arrays were originally passed in as parameters in order to determine where boundaries lay. 
The \textit{boundary} optimization now computes these values on-the-fly, again leading to fewer unnecessary computations.
This can be seen below left in Figure \ref{fig:optimizations}, where an on-the-fly mask is now used in conjunction with the original array instead of passing in a hard-coded mask.
This depiction shows that originally matrices of values of the room were having to be pre-padded manually to show where the boundary was.
Now, these grids can be padded on-the-fly without passing in extra data.

\begin{figure}[H]
\flushleft
 \resizebox{1\hsize}{!}{\includegraphics[width=17cm]{otherpapers/images/liftOptimisations.pdf}}
 \caption{Visual representation of the \textit{select} and \textit{boundary} optimizations in \textsc{lift}.}
  \label{fig:optimizations}
\end{figure}

\subsection{Optimizations for Rewrite Rules for Stencils}
\label{sec:twopointfive}
\begin{figure}[H]
\flushleft
\resizebox{1\hsize}{!}{\includegraphics{otherpapers/images/overlappingtilesactual2.pdf}}
% \resizebox{1\hsize}{!}{\includegraphics[width=17cm]{otherpapers/images/liftOptimisations.pdf}}
\caption{A visual representation of the 2.5D tiling optimization for 3D stencil codes.}
  \label{fig:twopointfiveD}
\end{figure}
The performance of 3D stencils, however, still lags behind optimized versions of the original benchmark, so current work is focusing on developing and formalizing stencil optimizations for 3D models, beginning with 2.5D tiling\cite{twopointfiveD}.
The intention is for this optimization (and any others added) to be encoded as a rewrite rule when it is finished. 
This is because it does not necessarily give better performance for all architectures and grid sizes. 
Thus, as a rewrite rule, it can be tested out for more optimal performance and be rejected where it does not provide that.
\par
For the 2.5D tiling optimization, previous results showed performance improvements of up to 15\% when using this method with local memory on GPUs for room acoustics benchmarks\cite{reesemasters}.
Figure \ref{fig:localMem} in  Section \ref{sec:perfcomp} shows these results.
However, this optimization has been used elsewhere successfully\cite{twopointfiveD} - it is not just limited to room acoustics simulations but can be used with any 3D time-stepping stencil.
This method can be described as an XY-tiling method that iterates sequentially over the Z dimension of the room.
This means that the Z index of a grid point is held constant while X and Y indices are calculated for a tile spanning the XY plane.
For subsequent iterations, the Z index is incremented and the next tile is updated.
Local memory is also utilized for points which are reused across tiles.
This method is shown visually in Figure \ref{fig:twopointfiveD}.
The large tiles represent the \textit{L} number of 2D tiles that comprise a $JxKxL$ sized 3D grid.
The smaller internal tiles represent the smaller grid that each thread computes over. 
The thread space is then divided up into two dimensions instead of three, where the third dimension (\textit{L}) is computed over sequentially.

To add this method in \textsc{lift}, the optimization needs to be decomposed into a \textsc{lift} language primitive that can be written in functional ways. 
We have done this by implementing a \textit{mapseq} followed by a \textit{slide}.
\textit{Mapseq} stands for ``map sequential.'' 
Whereas normally a map can apply a function in parallel, a \textit{mapseq} performs a function across a dimension sequentially. 
This means that the sequential loop for the Z-dimension is formed by the \textit{mapseq} and the neighborhoods to access the stencil (and reuse memory) are created by the \textit{slide}. 
How these primitives work independently is described in more detail in \cite{liftcgo}.


This work has proven challenging as many sequential algorithms do not have a natural one-to-one mapping in functional languages. 
Overcoming this has required several iterations of brainstorm, test, throw away, repeat.
While we currently have a solution available for simple 3D (and similarly 2D and 1D) stencils such as Jacobi, implementing this optimization specifically for room acoustics codes (or similar ``time-stepping'' stencils) requires more work. 
This is because the values in the stencil must correspond to previous values in the previous timestep which complicates the windows created for memory accesses.
We are still working on overcoming this issue by enabling shapes to be imprinted using this primitive, in the same fashion as the shape optimization as explained in Section \ref{sec:liftopt}.



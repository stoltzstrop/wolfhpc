\section{Performance and Productivity in Existing Approaches for Stencil Computations}
\label{sec:relatedwork}
%Current solutions
There are currently a wide range of potential frameworks and tools available that aim to provide higher levels of productivity, portability, performance and combinations therein which could accommodate stencil computations like 3D wave models.
At the low-level end (or libraries and tools that are much closer to machine level) there exist a number of libraries or language enhancements.
However, these low-level solutions do not provide productivity, so we will focus on high level solutions because this characteristic is crucial.
Higher-level abstraction solutions include skeleton frameworks, code generators, DSLs and others.
These high-level approaches focus more on distinct layers of abstraction that are far removed from the original codes and which generally aim to support a higher level of productivity for the programmer.
Many of these higher level approaches  even focus in particular on stencil applications (ie. Halide\cite{halide}, Pochoir\cite{pochoir} and Exastencils\cite{exastencils}).

\subsection{Algorithmic Skeletons}
Skeleton frameworks are a large subgroup of abstraction solutions developed for enhancing productivity.
These skeletons focus on the idea that many parallel algorithms can be broken down into pre-defined building blocks\cite{murraybook}.
Thus, an existing code could be embedded into a skeleton framework that already has an abstraction and API built for that algorithm type, such as a stencil.
These frameworks then simplify the process of writing complex parallel code (like OpenCL) by providing an interface which masks the low-level syntax and boilerplate.
A number of these skeleton frameworks have been developed, many of which are intended for grid-based applications, however few have been tested on larger simulation models.
Additionally, many of them lack 3D functionality, such as Skepu\cite{skepu} and SkelCL\cite{skelcl} which only support 1D and 2D stencils.
Some of the libraries also only target particular architectures - for example PSkel, which does support 3D stencils, but only ports to NVIDIA GPUs\cite{pskel}.
\subsection{Code Generators}
Code generators translate or compile source code from one language into another. 
They are a promising area in this field of research given that their modularity allows for flexibility between languages and platforms. 
Petabricks is an example of a code generator, which is actually a language and a compiler capable of auto-tuning over multiple pre-existing implementations of algorithms to tailor to a specific hardware\cite{petabricks}. 
Kokkos and SYCL are two other code generators that are gaining popularity, which compile down to CUDA and OpenCL respectively, and are tailored more towards general use and ease of programmability\cite{kokkos,sycl}.
Kokkos also has a  sophisticated memory mapping pattern for optimizing codes. 
SYCL  focuses more on bringing a simplified interface to OpenCL in C++.
None of these offer stencil-specific support for improving productivity, however, and they both miss out on automatic performance portability.
There is however, one code generation framework with a similar approach to \textsc{lift}: Delite\cite{delite} also utilizes the concept of ``parallel patterns,'' however still relies on hard-coded optimizations.

\subsection{Domain Specific Languages}
There are a large number of DSLs available, many of these focusing on stencils including: Exastencils, Halide, Pochoir and others. 
However, none of these support stencils with absorbing boundary conditions nor do they achieve full performance portability without heuristics.
Exastencils is a DSL developed at the University of Passau that aims to create a layered framework that uses domain specific optimizations to build performant portable stencil applications\cite{exastencils}.
Halide is another functional DSL with auto-tuning that specializes in abstracting stencils by separating algorithm from execution\cite{halide}.
Exastencils aims for performance portability, however its approach is through the use of a tree of heuristics. 
Halide focuses primarily on image processing stencils, which have less in common with 3D wave models.
Pochoir is limited to particular hardware. 
Ideally these DSLs could focus on abstraction, while another level of code addresses the performance portability problem.


\subsection{Limitations of Current Approaches}

 \par While  these high-level frameworks take fairly different approaches, none are without problems.
Skeletons enable ease of programmability and portability, but they alone are not enough to produce performance-portable code without heuristics, especially as they are often tied to a particular framework or architecture. 
A framework that remains de-coupled from specific implementations would avoid this problem.
For the higher-level code generation frameworks, performance results look promising in comparison to hand-tuned optimizations, though generally only small benchmarks have been tested or broad domains focused on. 
Some of the stencil-specific frameworks (like Halide) also focus mainly on images, simple stencils or other non-HPC specific domains. 
The main focus of DSLs also tends to be on productivity, which means that performance and portability often lags behind or codes are only performant for particular architectures.
These limitations mean that there currently are no solutions that have been shown to give 3D wave model simulations good performance, portability and productivity.


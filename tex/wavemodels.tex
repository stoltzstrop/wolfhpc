\section{Room Acoustics and Other 3D Wave Models}
\label{sec:wavemodels}

3D wave-based simulations are an important tool in physics for modeling the evolution of waves through space and time of various mediums. 
Room acoustics simulations are an example of such a simulation and simplified benchmarks of this type of simulation have been studied previously in more depth\cite{reesedafx}.  
Performance and bandwidth were compared across different implementations, as well as different data abstractions of the same implementation using a bespoke framework. 
This framework, called \textit{abstractCL} was developed for room acoustic simulations to test out different optimizations and data abstractions without rewriting the code.
It shows that productivity can be lifted for room codes, while retaining performance across different hardware.
The results of this research provide baseline comparison points for later work with the \textsc{lift} framework. 
As well as this, the development of this niche framework motivates the need for a completely different type of approach than just writing a library to work with room codes.


\subsection{Overview} 
\par

The finite difference time domain method (FDTD) is a widely used numerical approach for modelling of the 3D wave equation\cite{botteldooren}, which is shown in Equation \ref{fig:3Dwave}.

\begin{equation}
\frac{\partial^2 \Psi}{\partial t^2} = c^2\nabla^2 \Psi
\label{fig:3Dwave}
\end{equation}

Space is discritised into a three-dimensional grid of points, with data values resident at each point representing the acoustic field at that point.
The state of the system evolves through time-stepping: values at each point are repeatedly updated using ``finite differences'' of values in the neighborhood of that point.
This algorithm (also known as a stencil) updates points as determined by the choice of discretisation scheme for the partial differential operators in the wave equation \cite{bilbao2013large}.  
This approach can be computationally expensive, however parallelization techniques have shown great improvements in performance.
Though there has been progress in the development of techniques to exploit modern parallel hardware, much of it is low-level or tied to specific platforms. 
Ideally, scientific models should be able to run in a portable manner across different architectures while retaining performance and being straightforward to program.
\par
Room acoustics simulations were developed to model sound waves in an enclosed three dimensional space.
These simulations use first physical principals to represent the properties of  sound as it moves through space and time.
``Room codes'' use grids as data types that update values via the commonly used nearest-neighbors technique.  
The output of these simulations can provide composers or architects the ability to hear what a composition or noise would sound like in a space without actually being there or even having built it.
However, there are many other types of wave-based simulations, such as ground penetrating radar or lattice boltzman, which use similar techniques for more accurate modeling.
Thus, the results found here specifically for room acoustics codes could readily be applied to other, similar wave based models.
\subsection{Previous Work}
\par
Our previous research has shown that it is possible to develop more productive simulation codes such as room acoustics, while still achieving high performance across platforms.
A study was done across various architectures with a number of different implementations of the same room acoustics benchmark using different programming models to determine limiting factors and behavior.
Memory bandwidth was shown to be the limiting factor for this benchmark as can be seen that it runs fastest on the platform with the highest memory bandwidth (AMD R9 295X2).
In addition, a more productive, bespoke framework was developed to test out different data layouts and optimizations without rewriting the code. 

\subsubsection{Performance Comparison}
\label{sec:perfcomp}
\par
Four implementations of the same room acoustics benchmark (CUDA, OpenCL, abstractCL and targetDP) were run on six platforms (Intel Xeon CPU, Xeon Phi Knights Corner, NVIDIA K20, NVIDIA GTX 780, AMD R280 and AMD R9 259X2). 
Obviously not all versions were able to run on all platforms - CUDA and targetDP - are limited to NVIDIA platform GPUs. 
All other versions utilize OpenCL which can run on all selected platforms.
\textit{targetDP} is a low-level library for running simulation codes on NVIDIA GPUs, CPUs or Xeon Phis without rewriting the codes\cite{targetDP}.
\textit{abstractCL} is the developed framework tied to room acoustics simulations that we built using C++ macros and OpenCL to allow simplified swapping in and out of data layouts and optimizations\cite{reesedafx}. 
Specifications for the various platforms can be seen in Table \ref{tab:specsmatrix}. 

\par
Performance results of the runs of the four different versions can be seen in Figure \ref{fig:allTimings}.
The various colors indicate how much time was spent in which part of the code:  red for the time in the room update kernel, orange for the time spent in aggregating values at the receiver point kernel, green for the time spent copying data around and blue for all other miscellaneous time.
The graph is split up into six sections, one for each of the platforms run on, which is indicated at the top of each section of the graph.
Then, the bars in each section show the performance of the version of the code that has produced the result. 
Performance was generally comparable on all GPU platforms and more widely pronounced on the Xeon Phi and CPU (though getting reproducible results on the Xeon Phi proved to be tricky).
However performance across platforms differed tremendously, up to 40-50\% between GPUs.


\begin{table}[h]
\centering
    \resizebox{1.0\hsize}{!}{\begin{tabular}{|c|c|c|c|c|}
        \hline
          \textbf{Platform} &  \textbf{Number of Cores/} & \textbf{Peak Bandwidth (GB/s)} & \textbf{Peak GFlops} & \textbf{Memory (MB)}\\ 
           &  \textbf{Stream Processor} &  & (Double Precision) & \\ \hline \hline
          NVIDIA K20 & 2496 & 208 & 1175 & 5120 \\ \hline
          NVIDIA GTX 780 & 2304  & 288.4 & 165.7 & 3072 \\ \hline
          AMD R280     & 2048  & 288  & 870  & 3072 \\ \hline
          AMD R9 259X2 & 2816  & 320 & 716.67  & 4096 \\ \hline
          Xeon Phi 5110P    & 60  & 320 & 1011  & 8000 \\ \hline
          Intel Xeon E5-2670   & 32  & 51.2 & 166.4 & 126000 \\ \hline
    \end{tabular}}
    \caption{Platform Specification Table. This table shows the specifications of the various platforms used in previous work on comparing versions of a room acoustics simulation.} 
    \label{tab:specsmatrix}
\end{table}

\begin{figure}[h]
  \centering
 \resizebox{1\hsize}{!}{\includegraphics[width=17cm]{otherpapers/imagesdafx/allTimingsComparison512.pdf}}
  \caption{Performance timings for simple room acoustics benchmark implementations across various platforms using room sizes of 512x512x404 grid points.}
  \label{fig:allTimings}
\end{figure}

\par
In addition to comparisons of the original benchmark, comparisons were done with an optimized version of the room acoustics code. 
More about the 2.5D tiling optimization used in this version can be found in Section \ref{sec:twopointfive}.
These comparisons involved the use of local and image memory and through the use of \textit{abstractCL}, this could be swapped in and out easily without having to rewrite the whole benchmark.
Results of adding these optimizations can be seen in Figure \ref{fig:localMem}, where the blue graphs show more optimal memory usage than the original version shown in green which led to performance increases of up to 15\%. 


\begin{figure}[h]
\flushleft
 \resizebox{1\hsize}{!}{\includegraphics[width=17cm]{otherpapers/imagesdafx/localMemory512.pdf}}
\caption{Local memory optimizations for CUDA, OpenCL and abstractCL versions of the room acoustics benchmarks run for room sizes of 512x512x404 grid points.}
  \label{fig:localMem}
\end{figure}

\subsubsection{abstractCL}
\label{sec:absCL}
A new framework, \textit{abstractCL}, was created as a more productive solution for generating room simulation kernels on-the-fly, depending on the type of run a user wants to do. 
The type of variations can be between different data layouts of the grid passed in to represent the room, hardware-specific optimizations or both.
This is done through flags which swap in and out relevant files that include overloaded functions and definitions in the main algorithm itself. 
Certain functions must always be defined as dictated by a parent  class.
Those functions' implementations can be pulled in from different sources and concatenated together to create the simulation kernel before compilation. 
The data abstractions and optimizations  investigated for this project include: thread configuration settings, memory layouts and memory optimizations. 
Algorithmic changes can be introduced by adding new classes to the current template for more complicated codes.

\par
This framework runs similarly to the other benchmark versions, apart from that the kernel is created before the code is run (which creates some initial overhead).  
It was developed in C++ (due its built-in functionality for classes, templates, inheritance and strings) as well as OpenCL.
However, as shown in Figure \ref{fig:allTimings}, the framework produces code that is on par with hand-tuned OpenCL codes.
However it raises the level of productivity, which in previous work we have shown comparisons of to other versions using LOC (lines of code), where \textit{abstractCL} has significantly fewer\cite{reesemasters}.
\par
Optimizations must be tuned manually for each architecture, which is a limitation in its performance portability. 
Another limitation of this framework is that while it could be extended to other types of models, it is currently only designed for room acoustic simulations.
Additionally, it is still relatively low-level compared to other DSLs.
Finally, though it can provide some level of performance portability, this is done through hard-coded optimizations.
Our goal is to improve upon all of these issues with our modularized approach using the \textsc{lift} language.

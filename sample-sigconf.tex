\documentclass[sigconf]{acmart}

\usepackage{booktabs} % For formal tables

\usepackage{listings}
\lstset{%
	numbers = left,
	frame = single,
	breaklines = true,
	showspaces = false,
	basicstyle = \ttfamily\small,%\footnotesize,
  %backgroundcolor = \color{gray!10},
	tabsize = 1,
	mathescape,
  captionpos=b,
  xleftmargin=10pt,
}

\newcommand{\squeezeup}{\vspace{-5.5mm}}

% Copyright
\setcopyright{none}
%\setcopyright{acmcopyright}
%\setcopyright{acmlicensed}
%\setcopyright{rightsretained}
%\setcopyright{usgov}
%\setcopyright{usgovmixed}
%\setcopyright{cagov}
%\setcopyright{cagovmixed}


% DOI
%\acmDOI{10.475/123_4}

% ISBN
%\acmISBN{123-4567-24-567/08/06}

%Conference
\acmConference[WOLFHPC]{WOLFHPC}{November 2017}{Denver, USA} 
\acmYear{2017}
\copyrightyear{2017}

%\acmPrice{15.00}


\begin{document}
\title{A Modular Approach to Performance, Portability and Productivity for 3D Wave Models}
%\titlenote{Produces the permission block, and}
%  copyright information}
\subtitle{WOLFHPC Workshop}
%\subtitlenote{The full version of the author's guide is available as
  %\texttt{acmart.pdf} document}


\author{Larisa Stoltzfus}
\affiliation{%
  \institution{School of Informatics}
  \institution{University of Edinburgh}
}
\email{larisa.stoltzfus@ed.ac.uk}

\author{Christophe Dubach}
\affiliation{%
  \institution{School of Informatics}
  \institution{University of Edinburgh}
}

\author{Michel Steuwer}
\affiliation{%
  \institution{School of Computing Science}
  \institution{University of Glasgow}
}
%\email{webmaster@marysville-ohio.com}

\author{Alan Gray}
\affiliation{%
  \institution{Edinburgh Parallel Computing Centre}
}
%\email{larst@affiliation.org}

\author{Stefan Bilbao}
\affiliation{%
  \institution{School of Music}
  \institution{University of Edinburgh}
}
\renewcommand{\shortauthors}{Larisa Stoltzfus et. al}

% The default list of authors is too long for headers}
%\renewcommand{\shortauthors}{B. Trovato et al.}


%The parallel programming landscape has become so vast and complex that developing simulation codes which are performant, portable and easily programmable requires compromising somewhere. 
%Existing approaches have focused on abstracting away parallelisation instructions from algorithms, using libraries to hide details of hardware specific code and low-level syntax. 
%However, no current solutions provide all three of performance, portability and productivity.
%In addition, many have been tested only on simple benchmarks or focus on specific hardware optimisations, resulting in a lack of performance portability.
%
%This project proposes developing a modular solution for  providing performance, portability and productivity from the top down, beginning with algorithms of interest: 3D wave-based simulations discretised to stencils.
%By employing the so-called ``assembly language of HPC'' \textsc{Lift}\cite{lift}\cite{liftcgo}, we propose to develop and enhance this intermediary language to enable computation and optimisation for complex 3D stencils, in particular those with absorbing boundary conditions as found in wave simulations.
%We then propose to harness the abstractions of existing stencil DSLs to create a modularised methodology for wave-based simulation development. 
%This will involve adapting a top-level DSL to compile into \textsc{Lift}, which can then use \textsc{Lift}'s composability and rewrite rules to compile these models into performant, portable and  productive code.\\
%Research has shown that it is possible to develop more productive and performance portable codes for room acoustics simulations which simplify the problems of writing programmable and performant code across different platforms.
%
%    % Future Plans 
%This work can be extended to other physical simulations using similar algorithms. 
%Going forward, research could also be done to extend this work into a DSL for grid-based simulations (like room acoustics). 
%Furthermore, this DSL could be  brought into existing frameworks to compile HPC simulations to architecture specific optimised code. 


\begin{abstract}
%Introduction / State the problem
%Background
%Methods
%Results
%Conclusions

    %Overview

    %Problem
The HPC hardware landscape is growing increasingly complex in order to meet demands in scientific computing for greater performance.
In recent years there has been an explosion of parallel devices coming on to the scene: GPUs, Xeon Phis and FPGAs to name but a few examples. 
As of writing, even the current leading supercomputer, Sunway TianhuLight, uses its own bespoke on-chip accelerators\cite{tianhu}.  
Available programming models, however, lag behind and are not currently able to provide the necessary tools for running scientific codes across platforms in ways that are performant, portable and productive.

\par
This environment creates a plethora of challenges for computational scientists of which we focus on two: first the need for a high level of productivity for codes that still get good performance and second consistently getting good performance across platforms - the ``performance portability'' problem.
Existing solutions tend to be either not productive but provide good performance or focus on high-level abstractions requiring heuristics to get good performance (often which are tied to particular platforms).
While some current approaches raise the productivity level, they are often trying to solve the same problems over and over or trying to solve too many issues for a niche domain.
In addition, many of these approaches have only been tested on simplistic benchmarks, which can lose critical functionality of real-world simulation codes.
We instead propose a modular approach using existing frameworks to target these issues separately: a high-level DSL to target the productivity problem compiling into an IR language which addresses the performance portability problem. 

\par
Our previous research has shown that the development of more productive and performance portable codes for room acoustics simulations is possible.
Preliminary results  using the intermediary parallel language \textsc{lift}\cite{lift} confirm that this framework is capable of handling complex stencils. 
Further developing \textsc{lift} and targeting existing stencil-focused DSLs will create a simple, modularized approach which harnesses and expands existing functionality instead of trying to reinvent the wheel.
This modular approach can then be used as an example to extend to other physical simulations using similar algorithms. 

\end{abstract}

%
% The code below should be generated by the tool at
% http://dl.acm.org/ccs.cfm
% Please copy and paste the code instead of the example below. 
%
%\begin{CCSXML}
%<ccs2012>
% <concept>
%  <concept_id>10010520.10010553.10010562</concept_id>
%  <concept_desc>Computer systems organization~Embedded systems</concept_desc>
%  <concept_significance>500</concept_significance>
% </concept>
% <concept>
%  <concept_id>10010520.10010575.10010755</concept_id>
%  <concept_desc>Computer systems organization~Redundancy</concept_desc>
%  <concept_significance>300</concept_significance>
% </concept>
% <concept>
%  <concept_id>10010520.10010553.10010554</concept_id>
%  <concept_desc>Computer systems organization~Robotics</concept_desc>
%  <concept_significance>100</concept_significance>
% </concept>
% <concept>
%  <concept_id>10003033.10003083.10003095</concept_id>
%  <concept_desc>Networks~Network reliability</concept_desc>
%  <concept_significance>100</concept_significance>
% </concept>
%</ccs2012>  
%\end{CCSXML}

%\ccsdesc[500]{Computer systems organization~Embedded systems}
%\ccsdesc[300]{Computer systems organization~Redundancy}
%\ccsdesc{Computer systems organization~Robotics}
%\ccsdesc[100]{Networks~Network reliability}


\keywords{Code generation, heterogenous computing, DSLs, stencils}


\maketitle
\input{tex/intro} 
\input{tex/related}
\input{tex/wavemodels}
\input{tex/liftlanguage}
%\input{tex/previous}
\input{tex/lift}
\input{tex/future}
\input{tex/conclusions}
\input{tex/acknowledge}

%\input{wolfhpc}
%\input{samplebody-conf}

\bibliographystyle{ACM-Reference-Format}
\bibliography{wolfhpc} 
%\bibliography{sample-bibliography} 

\end{document}

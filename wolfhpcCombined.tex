
\section{Introduction}
% importance of simulations generally
\par
Computer simulations are a critically important tool in scientific fields that bridge theory with reality. 
Many of these simulations - such as 3D wave-based models like room acoustics\cite{craigthesis} or ground penetrating radar\cite{gprmax} -  are complicated to model and even more difficult to abstract about.
However, such simulations are integral to predicting the properties and behavior of the physical world around us. 
As such, the ability to program simulations which can perform well across a range of different architectures is of growing importance.

% why simulations need all three of perf port prod and why they can't get them
\par
Computers are  moving towards parallel architectures as greater performance can no longer be achieved through a single core.
The types of architectures available are changing and increasing to meet demands for performance for scientific codes which have traditionally been run on CPUs.
It is therefore crucial to accommodate portability across old and new platforms in order to avoid having to rewrite code bases as new platforms emerge. 
Currently, many scientific groups utilize multiple code bases, which can be error-prone and time-consuming to maintain.
Computational scientists should be able to focus on their own research and not require HPC expertise for re-tuning and rewriting codes for every emergent platform they might want to try out.
Furthermore, even where codes can port from one architecture to another, there is no guarantee they will retain the same performance on the new platform - this is an additional, important problem of ``performance portability.'' 


 %breakdown of paper 
%% Motivation :
\par

This project will approach these issues with a  modularized solution to the development of performant, portable and productive 3D wave models using existing frameworks.
A large number of high-level parallel methodologies currently exist which focus on stencil applications, however they  neglect to manage absorbing boundary conditions, which are critical to obtaining accurate results for 3D wave models and none provide all three of performance, portability and productivity.
This will be discussed further in Section \ref{sec:relatedwork}.
Next, some necessary background is introduced in Section \ref{sec:background}. 
Then, some motivation for this project through the expansion of some previous related work is discussed in Section \ref{sec:prevwork}.
Current and ongoing work is discussed at length in Section \ref{sec:modelinlift}.
Future work involving the \textsc{lift} framework is covered in Section \ref{sec:future}.
Finally, the paper concludes in Section \ref{sec:conclusion}.


\section{Related Work}
\label{sec:relatedwork}
%Current solutions
There are currently a wide range of potential frameworks and tools available that aim to provide higher levels of productivity, portability, performance and combinations therein.
At the low-level end (or libraries and tools that are much closer to machine level, thus providing less portability) there exist a number of libraries or language enhancements.
However, low-level solutions do not provide superior programmability, so for brevity, we will focus on high level solutions.
With higher levels of abstraction, there are a wider range of possibilities including  DSLs, code generators, skeleton libraries, combinations therein and others.
These high-level approaches focus more on distinct layers of abstraction that are far removed from the original codes and which generally aim to support a higher level of programmability.
Many of these higher level approaches  even focus on stencil applications (ie. Halide\cite{halide}, Pochoir\cite{pochoir}, Exastencils\cite{exastencils} and so on).
However, these stencil solutions are limited in the types of stencils they focus on and none offer performance portability.

\subsection{Algorithmic Skeletons}
Skeleton frameworks are a large subgroup of abstraction frameworks developed for enhancing productivity.
These skeletons focus on the idea that many parallel algorithms can be broken down into pre-defined building blocks\cite{murraybook}.
Thus, an existing code could be embedded into a skeleton framework that already has an abstraction and API built for that algorithm type, such as a stencil.
These frameworks then simplify the process of writing complex parallel code (like OpenCL) by providing an interface which masks the low-level syntax and boilerplate.
A number of these skeleton frameworks have been developed, many of which are intended for grid-based applications, however few have been tested on larger simulation models.
Additionally, many of them lack 3D functionality, such as Skepu\cite{skepu} and SkelCL\cite{skelcl} which only support 1D and 2D stencils.
Some of the libraries also only target particular architectures - for example PSkel, which does support 3D stencils, but only ports to NVIDIA GPUs\cite{pskel}.
\subsection{Code Generators}
A code generator can either be a type of compiler or more of a source to source language translator. 
They are a promising area in this field of research given that their modularity allows for flexibility between languages and platforms. 
Petabricks is another example of a code generator, which is actually a language and a compiler capable of auto-tuning over multiple pre-existing implementations of algorithms to tailor to a specific hardware\cite{petabricks}. 
Kokkos and SYCL are two other code generators that are gaining popularity, which compile down to CUDA and OpenCL respectively, and are tailored more towards general use and ease of programmability\cite{kokkos}\cite{sycl}.
Kokkos also has a  sophisticated memory mapping pattern for optimizing codes. 
SYCL  focuses more on bringing a simplified interface to OpenCL in C++.

\subsection{Other Approaches}
Other higher-level approaches include functional DSLs with auto-tuning\cite{zhang_DSL}, rewrite rules\cite{spiral}, skeleton frameworks combined with auto-tuning\cite{skeleton1}, code generators and many others including the examples below.
Liquid Metal is a project started at IBM to develop a new programming language purpose built to tailor to heterogeneous architectures\cite{liquidmetal}.
Exastencil is a DSL developed by a group at the University of Passau that aims to create a layered framework that uses domain specific optimizations to build performant portable stencil applications\cite{exastencils}.
Halide is another functional DSL with auto-tuning that specializes in abstracting stencils by separating algorithm from execution\cite{halide}.
Both Exastencil and Halide could be potentially be quite useful as abstractions for room acoustics simulations.

\subsection{Limitations of Current Approaches}

 \par While  these high-level frameworks take fairly different approaches, none are without problems.
Skeletons enable ease of programmability and portability, but they alone are not enough to produce performance-portable code especially as they are often tied to a framework or architecture. 
A framework that remains de-coupled from specific implementations would avoid this problem.
For the higher-level code generation frameworks, results look promising in comparison to hand-tuned optimizations, though generally only small benchmarks have been tested or broad domains focused on. 
Some of the stencil-specific frameworks (like Halide) also focus mainly on images, simple stencils or other non-HPC specific domains. 
Many also often focus on getting good performance of particular hardware instead of the algorithms that would use the framework.
These limitations mean that there currently are no DSLs or frameworks developed that have been shown to give room acoustics simulations good performance, portability and productivity.

\par Instead of writing yet another stencil library or DSL to address the shortcomings of current solutions, this project aims to take advantage of what is available already and create a modularized approach to simulation development for HPC using existing DSLs and the \textsc{Lift} intermediary language framework.
In an ideal world, DSLs would use a common compiler, so that the writers of these languages could focus on the needs of their specific domain in the abstraction layer at the top instead of also having to manage hardware-specific low-level optimizations.
This is where the \textsc{lift} language provides great utility.
\textsc{lift} is designed to be used as an intermediate layer targeted by DSLs to handle low-level implementation and optimization.


\section{Background}
\label{sec:background}

Two of the main topics in this paper include room acoustics simulations and the \textsc{lift} language.
While this end goal of this project is to provide solutions to 3D wave models generally, previous and current work has predominantly focused on room acoustics simulations. 
This work has focused on analysis more than development, however current work is now using the \textsc{lift} language as discussed later in Section \ref{sec:modelinlift}.
This language is being developed as an intermediary representation of parallel algorithms. 
Both are described in more detail below. 

\subsection{Room Acoustics and Other 3D Wave Models}
Room acoustics simulations were developed to model sound waves in an enclosed three dimensional space.
The simulations use first physical principals to represent the properties of  sound as it moves through space and time.
These ``room codes'' use grids as data types that update values via the very a commonly used nearest-neighbors technique.  
The output of these simulations can provide composers or architects the ability to hear what a composition or noise would sound like in a space without actually being there or even having built it.
\par

3D wave-based simulations are an important tool in physics for modeling the evolution of waves through space and time of various mediums. 
The finite difference time domain method (FDTD) is a widely used numerical approach for modeling of the 3D wave equation\cite{botteldooren}, which is shown in Equation \ref{fig:3Dwave}.

\begin{equation}
\frac{\partial^2 \Psi}{\partial t^2} = c^2\nabla^2 \Psi
\label{fig:3Dwave}
\end{equation}

Space is discritised into a three-dimensional grid of points, with data values resident at each point representing the acoustic field at that point.
The state of the system evolves through time-stepping: values at each point are repeatedly updated using ``finite differences'' of values in the neighborhood of that point.
This algorithm (also known as a stencil) updates points as determined by the choice of discretisation scheme for the partial differential operators in the wave equation \cite{bilbao2013large}.  
This approach can be computationally expensive, however parallelization techniques have shown great improvements in performance.
Though there has been progress in the development of techniques to exploit modern parallel hardware, much of it is low-level or tied to specific platforms. 
Ideally, scientific models should be able to run in a portable manner across different architectures while retaining performance and being straightforward to program.

\subsection{The Lift Language}

%Advantages: Functional, reusable, rewrite rules. High level primitives -> low-level optimized code.

The \textsc{lift} language was developed as a modular solution to the performance portability problem\cite{liftcgo}. 
The idea is that by separating HPC programming into separate levels of abstraction, then each level can be restricted to its particular purpose. 
The language introduces three key concepts: functional, reusable high-level primitives; rewrite rules describing exchangeable relationships between primitives and a code generating search space which determines the best version of a kernel to run.
At the higher level, the language is composed of ``reusable primitives,'' written in a functional style. 
Algorithms for a wide variety of applications (including stencils) can be rewritten as compositions of these primitives. 
Figure \ref{fig:primitives} shows a sampling of some of these primitives and how they work.
``Rewrite rules'' describe a variety of transformations of a given algorithm decomposition, which are provable to return the same result.
A search space of these rewrite rules is automatically explored to optimize codes for a particular platform.
Kernels are then generated in OpenCL for all the different ``rewrites'' until an optimal program is found for a particular platform. 
Currently, the only backend \textsc{lift} supports is OpenCL, however the modular design of the language makes it easily extensible to other parallel programming frameworks.

\begin{figure}
  \centering
 \resizebox{1\hsize}{!}{\includegraphics[width=17cm]{algorithmicprimitivescut.pdf}}
 \caption{Algorithmic Primitives in the \textsc{Lift} Language}
  \label{fig:primitives}
\end{figure}


%\subsection{Background}
% Physical simulations and Stencils 

%Motivation?
\section{Previous Work}
\label{sec:prevwork}

\par
Previous research has shown that it is possible to develop more productive and performance portable codes for simulation codes such as room acoustics\cite{reesedafx}.
A study was done across various architectures with a number of different implementations of the same room acoustics benchmark using different programming models to determine limiting factors and behavior.
Memory bandwidth was shown to be the limiting factor for this benchmark as can be seen that it runs fastest on the platform with the highest memory bandwidth (AMD R9 295X2).
In addition, a more productive, bespoke framework was developed to test out different data layouts and optimizations. 

\subsection{Performance Comparison of Software}
\par
Four implementations of the same room acoustics benchmark (CUDA, OpenCL, abstractCL and targetDP) were run on six platforms (Intel Xeon CPU, Xeon Phi Knights Corner, NVIDIA K20, NVIDIA GTX 780, AMD R280 and AMD R9 259X2). 
Obviously not all versions were able to run on all platforms - CUDA and targetDP which uses CUDA on GPUs - are limited to NVIDIA platform GPUs. 
All other versions utilize OpenCL which can run on all selected platforms.
\textit{targetDP} is a low-level library for running simulation codes on NVIDIA GPUs, CPUs or Xeon Phis without rewriting the code\cite{targetDP}.
\textit{abstractCL} is a framework tied to room acoustics simulations that we built using C++ macros and OpenCL to allow simplified swapping in and out of data layouts and optimizations\cite{reesedafx}. 
More about \textit{abstractCL} can be found in Section \ref{sec:absCL}.
Specifications for the various platforms can be seen in Table \ref{tab:specsmatrix}. 
Performance was generally comparable on all GPU platforms and more widely pronounced on the Xeon Phi and CPU (though getting reproducible results on the Xeon Phi proved to be tricky).
However performance across platforms differed tremendously, up to 40-50\% between GPUs as can be seen in Figure \ref{fig:allTimings}.

\begin{table}[H]
\centering
    \resizebox{1.0\hsize}{!}{\begin{tabular}{|c|c|c|c|c|}
        \hline
          \textbf{Platform} &  \textbf{Number of Cores/} & \textbf{Peak Bandwidth (GB/s)} & \textbf{Peak GFlops} & \textbf{Memory (MB)}\\ 
           &  \textbf{Stream Processor} &  & (Double Precision) & \\ \hline \hline
          NVIDIA K20 & 2496 & 208 & 1175 & 5120 \\ \hline
          NVIDIA GTX 780 & 2304  & 288.4 & 165.7 & 3072 \\ \hline
          AMD R280     & 2048  & 288  & 870  & 3072 \\ \hline
          AMD R9 259X2 & 2816  & 320 & 716.67  & 4096 \\ \hline
          Xeon Phi 5110P    & 60  & 320 & 1011  & 8000 \\ \hline
          Intel Xeon E5-2670   & 32  & 51.2 & 166.4 & 126000 \\ \hline
    \end{tabular}}
    \caption{Specification Table. This table shows the specifications of the various platforms used in previous work.} 
    \label{tab:specsmatrix}
\end{table}

\begin{figure}
  \centering
 \resizebox{1\hsize}{!}{\includegraphics[width=17cm]{otherpapers/imagesdafx/allTimingsComparison512.pdf}}
  \caption{Performance timings for simple room acoustics benchmark implementations across various platforms using room sizes of 512x512x404 grid points.}
  \label{fig:allTimings}
\end{figure}


\subsubsection{abstractCL}
\label{sec:absCL}
\textit{abstractCL} was created to make room simulation kernels on-the-fly, depending on the type of run a user wants to do. 
The type of variations can be between different data layouts of the grid passed in to represent the room, hardware-specific optimizations or both.
This is done through swapping in and out relevant files that include overloaded functions and definitions in the main algorithm itself. 
The data abstractions and optimizations  investigated for this project include: thread configuration settings, memory layouts and memory optimizations. 
Algorithmic changes can be introduced by adding new classes to the current template for more complicated codes.
\par
abstractCL works through the use of flags which determine what version should be run. 
Certain functions must always be defined as dictated by a parent  class.
However, those functions' implementations can be pulled in from different sources and concatenated together to create the simulation kernel before compilation. 
This framework runs similarly to the other benchmark versions, apart from that the kernel is created before the code is run (which creates more initial overhead).  
It was developed in C++ (due its built-in functionality for classes, templates, inheritance and strings) as well as OpenCL.

\subsection{Data Abstraction Comparison}
\par
\textit{abstractCL} was developed to allow for different data layouts and optimizations to be tested out quickly and easily. 
Several data layouts were tested out showing a range of performance variation across platforms. 
Figure \ref{fig:DataStructures} shows the results of this experiment. 
This test showed how careful one must be about abstractions, even at the low-level. 
It is difficult to program high-level abstractions with good performance without some kind of code generation. 
\begin{figure}[H]
  \center
 \resizebox{1.1\hsize}{!}{\includegraphics[width=17cm]{otherpapers/imagesmasters/structComparisonKernel.pdf}}
  \caption{Comparison of performance of various data structures across different platforms for a room size of 256x265x202. 
  %A guide to the different layout versions can be found in Table \ref{tab:datalayout}.
  }
  \label{fig:DataStructures}
\end{figure}


\section{3D Wave Model Development in the \textsc{Lift} Framework}
\label{sec:modelinlift}
\par
Current work has involved enhancing \textsc{lift} to accommodate room acoustics stencils. 
Preliminary results were poor, so several iterations of improvements have been added. 
Codes now run with comparable results to the original hand-written versions.
However, these codes still lag behind the optimized hand-tuned benchmark version.
Thus, 2.5D-tiling and other optimizations are being investigated as further improvements as well. 

\subsection{Updating Lift to Accommodate Complex 3D Stencils}
\label{sec:liftopt}
Preliminary results have shown that \textsc{Lift} is capable of expressing stencils of varying types and sizes\cite{bastianthesis}.
In particular, simplified room acoustics simulations have been thoroughly investigated in the framework and a number of other 2D and 3D stencil benchmarks have also been implemented. 
Additionally, ground penetrating radar algorithms are also being implemented in the language as they have similar wave behavior to room acoustics. 
Though they are modeled using the 3D wave equation and have absorbing boundary conditions, they require modeling both the electric and magnetic fields interacting with each other.

The implementation of the room acoustic stencils involved two stages of optimizations: adding a \textit{select} and a \textit{boundary} optimization. 
Originally, entire neighborhoods of values were being computed over instead of just values for the stencil, leading to needless memory accesses and extraneous computations. 
The \textit{select} optimization solved this issue by pulling out only those values which were used in the stencil calculation.
Additionally, masks of the same size as input arrays were originally passed in as parameters in order to determine where boundaries lay. 
The \textit{boundary} optimization now computes this value on the fly, again leading to fewer unnecessary computations.
A more visual representation of these optimizations is shown in Figure \ref{fig:optimizations}.
Results of adding these optimizations can be seen in Figure \ref{fig:liftCompare}, where it can be seen that the original version of the kernel was very far from the benchmark.


\begin{figure}[H]
\flushleft
 \resizebox{1\hsize}{!}{\includegraphics[width=17cm]{otherpapers/images/liftOptimisations.pdf}}
 \caption{Visual representation of the \textit{select} and \textit{boundary} optimizations in \textsc{lift}.}
  \label{fig:optimizations}
\end{figure}

\begin{figure}[H]
\flushleft
 \resizebox{1\hsize}{!}{\includegraphics[width=17cm]{otherpapers/images/liftVersionsComparison512.pdf}}
 \caption{Comparison of optimization versions in the \textsc{lift} framework implementation of room acoustic benchmark with grid size of  512x512x404 grid points.}
  \label{fig:liftCompare}
\end{figure}

\subsection{Optimizations for Rewrite Rules for Stencils}
The performance of 3D stencils, however, still lags behind optimized versions of the original benchmark, so currently we have been working to develop and formalize stencil optimizations for 3D models, beginning with 2.5D tiling\cite{twopointfiveD}.
This method can be described as an XY-tiling method that iterates sequentially over the Z dimension of the room.
This means that the Z index of a grid point is held constant while X and Y indices are calculated for a tile spanning the XY plane.
For subsequent iterations, the Z index is incremented and the next tile is updated.
Local memory is also utilized for points which are reused across tiles.
Previous results showed performance improvements of up to 15\% when used with local memory on GPUs for room acoustics benchmarks\cite{reesemasters}.

To add this method in \textsc{lift}, the optimization needs to be decomposed into a primitive or primitive that can be written in functional ways. 
We have done this by implementing a \textit{mapseq} followed by a \textit{slide}. 
This means that the sequential loop for the Z-dimension is formed by the \textit{mapseq} and the neighborhoods to access the stencil (and reuse memory) are created by the \textit{slide}. 
How these primitives work independently is described in more detail in \cite{liftcgo}.
A visual perspective of how this comes together can be seen in Figure \ref{fig:twopointfiveD}.

\begin{figure}[H]
\flushleft
\resizebox{1\hsize}{!}{\includegraphics{otherpapers/images/tileswiththingout.pdf}}
% \resizebox{1\hsize}{!}{\includegraphics[width=17cm]{otherpapers/images/liftOptimisations.pdf}}
\caption{A visual representation of the 2.5D tiling optimization for 3D stencil codes.}
  \label{fig:twopointfiveD}
\end{figure}

This work has proven challenging as many ``naturally'' sequential algorithms do not have a natural one-to-one mapping in functional languages. 
Overcoming this has required several iterations of brainstorm, test, throw away, repeat.
While we currently have a solution available for simple 3D (and similarly 2D and 1D) stencils such as Jacobi, implementing this optimization specifically for room acoustics codes (or similar ``time-stepping'' stencils) requires more work. 
This is because the values in the stencil must correspond to previous values in the previous timestep, so in the \textsc{lift} language tuples are used, complicated the windows created for memory accesses.
We are still working on overcoming this issue by enabling shapes to be imprinted using this primitive, in the same fashion as the shape optimization as explained in Section \ref{sec:liftopt}.

\begin{figure}[H]
\flushleft
 \resizebox{1\hsize}{!}{\includegraphics[width=17cm]{otherpapers/imagesdafx/localMemory512.pdf}}
\caption{Local memory optimizations for CUDA, OpenCL and abstractCL versions of the room acoustics benchmarks run for room sizes of 512x512x404 grid points.}
  \label{fig:localMem}
\end{figure}



\section{Future Work}
\label{sec:future}

So far only simplified versions of 3D wave models have been implemented in \textsc{lift}. 
Thus, how to best abstract out absorbing boundary conditions needs to be investigated in more detail and primitives to accommodate these conditions need to be designed and added.
Additionally, a stencil-based DSL needs to be extended to compile into the \textsc{Lift} language for use as a front end.
As well as these larger contributions, smaller issues such as targeting multiple cores or GPUs and iterative stencils need to be explored further. 
\subsection{Absorbing Boundary Conditions}
So far the room acoustics benchmarks used in this project have been ``state-free'' or in other words using constant values at the boundary.
While this produces a usable model, for better accuracy states need to be maintained at the boundary to model the absorption of waves there. 
These need to be retained for all of the timesteps being used (so two in the current benchmark, but at least three in more advanced codes) and updated accordingly for each iteration. 
This is difficult to model both because it makes the algorithm more computationally intense, but also makes memory accesses for the states non-contiguous. 
\subsection{DSL Extension}
In its current state, \textsc{Lift} is not a productive framework for use by computational scientists.
Though it does have ``high-level'' abstractions, these are not intended to be programmed directly. 
The ideal use for \textsc{lift} is as an intermediate language: write the algorithm in a higher level DSL to compile into \textsc{lift}, which will then compile down to hardware. 
For this, the most appropriate DSL should be chosen for room acoustics and other 3D wave models with absorbing boundary conditions.
First it will need to be extended to handle these advanced boundary conditions. 
Then, it will need to be adapted to compile into the \textsc{lift} language.
This will create a modularized pipeline where each stage can be tuned to focus on its part. 
That is, not trying to do too much.

\subsection{Updating Lift}
Currently \textsc{lift} is missing a few integral features that would allow for large scale simulations to run. 
First of all, iterative stencils are not a natural fit to the language. 
This is crucial for timestepping models like room acoustics and ground-penetrating radar which swap arrays of values corresponding to snapshots in time at each iteration.
Secondly, many large scale simulations require the ability to program across multiple devices. 
This is not something that \textsc{lift} currently supports, but would be necessary for widespread adoption.
Additionally it would be ideal for \textsc{lift} to accommodate more backends. 
Currently, it only supports OpenCL, which while portable to many devices is not a universal answer.






\section{Conclusion}
\label{sec:conclusion}
This work aims to provide a modular, reusable methodology for developing performance portable and easily programmable physical simulation models, in particular 3D wave models with absorbing boundary conditions.
Developing this will involve extending the \textsc{lift} framework to accommodate complex 3D stencils and harnessing a DSL to compile into the \textsc{lift} language.
Following suit, other types of DSLs and algorithms can use a similar workflow.
Ideally, this will lead by example to other opportunities for performance portable and programmable code for scientific models targeting HPC systems.
%

\section{Acknowledgments}
This work was supported in part by the EPSRC Centre for Doctoral Training in
Pervasive Parallelism, funded by the UK Engineering and Physical Sciences Research
Council (grant EP/L01503X/1) and the University of Edinburgh.

%Make sure that your PDF file does not contain page numbers, is
%up to 4 pages long and is formatted for an A4 page.
%Upload the final version before June 8 on the website.


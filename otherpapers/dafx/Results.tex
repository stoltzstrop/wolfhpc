\section{Results}
\label{sec:results}

In this section we present the results of comparing the different room acoustics model implementations described previously as run on the selected hardware platforms.
 We first present the best performing results on each architecture to provide an overall assessment of the hardware.
 We then compare the applicability, performance and portability of these different runs.
 We go on to show that memory bandwidth is critical to achieving good  performance.
Finally, we describe the effect of optimisations and extend results to a more complex version of the codes.


\subsection{Overall Performance Comparison Across Hardware}
\label{sec:bestperformance}

\begin{figure}[ht]
\center
\centerline{\includegraphics[scale=0.3]{images/use/fastest512.pdf}}
\caption{\label{fig:fastest}{\it Original fastest (optimised) versions across platforms for simple room acoustics simulation of room size 512$\times$512$\times$404. The timings shown produce 100ms of sound.}}
\end{figure}

In this section we give an overview of performance achieved across the different hardware platforms, to assess the capability of the hardware for this type of problem.
 Figure \ref{fig:fastest} shows the time taken for the test case where we choose the best performing existing software on each architecture.
 It can be seen that the GPUs show similar times, with the AMD R9 295X2 performing fastest.
 This result translates to 8466 megavoxels updates per second.
 Another point of interest is that the consumer-class NVIDIA GTX780 is significantly faster for the CUDA implementation than the K20, even though it has many times lower double precision floating point capability.
 This is because, as discussed further in Section \ref{sec:bandwidth}, memory bandwidth is more important that compute for this type of problem.
 The Xeon Phi is seen to offer lower performance that the GPUs, but remains faster than the traditional CPU.

%% \begin{table}[ht]
%%     \caption{\itshape Table of best performance of simple room acoustics simulations (size: 512x512x404 points)achieved in megavoxels per second.}
%% 	\centering
%% 	\begin{tabular}{|c|c|c|}
%% 		\hline
%%                 \textsc{platform} & \textsc{language} & \textsc{MVox/s}\\\hline
%%                 AMD R9 295X2&opencl&8466.1669928054\\
%%                 AMD R280 &opencl&6860.3367278111\\
%%                 NVIDIA GTX780&cuda&7048.5309894886\\
%%                 NVIDIA K20&cuda&5091.3267006564\\
%%                 Xeon Phi&abstractCL&1798.5272642834\\
%%                 Intel Xeon E5&targetDP&1127.4284049071\\\hline
%% 	\end{tabular}
%% 	%
%% 	\label{tab:mvox}
%% \end{table}


\subsection{Performance Comparison of Software Versions}
\begin{figure}[ht]
\center
\centerline{\includegraphics[scale=0.3]{images/use/allTimingsComparison512.pdf}}
\caption{\label{fig:AllTimings}{\it Original timings of the simple room acoustics simulation for room size 512$\times$512$\times$404 on all architectures tested. The timings shown produce 100ms of sound.}}
\end{figure}

In this section we analyse the differences in performance resulting from running different software frameworks on a particular platform.
 In Figure \ref{fig:AllTimings}, it can be seen how the performance depends on the software version.
 The main result is that for each architecture the timings are comparable, in particular in comparison to the \textit{abstractCL} version.
 On the NVIDIA GPU, there is a small overhead for the portable frameworks (OpenCL, abstractCL and targetDP) relative to use of CUDA, but we see this as a small price to pay for portability to the other platforms.
 In particular, the newly developed framework abstractCL shows comparable performance to the original benchmarks and those written in OpenCL, indicating that it is possible to build performant, portable and productive room acoustics simulations across different hardware.


\subsection{Hardware Capability Discussion}
\label{sec:bandwidth}

\begin{figure}[ht]
\center
\centerline{\includegraphics[scale=0.3]{images/use/allBandwidth512.pdf}}
\caption{\label{fig:AllBandwidths}{\it Data Throughput for different versions of the room acoustics benchmark across platforms for room size 512$\times$512$\times$404. }}
\end{figure}

In this section, we further analyse the observed performance in terms of the characteristics of the underlying hardware.
The \textsc{Roofline} model, developed by Williams et al. \cite{williams2009roofline}, can be used to determine for a given application the relative importance of floating point computation and memory bandwidth capabilities.
 It uses the concept of ``Operational Intensity'' (OI): the ratio of operations (in this case double precision floating point operations) to bytes accessed from main memory.
 The OI, in Flops/Byte, can be calculated for each computational kernel.
 A similar measure (also given in Flops/Byte and known as the ``ridge point''), exists for each processor: the ratio of peak operations per second to the memory bandwidth of the processor.
 Any kernel which has an OI lower than the ridge point is limited by the memory bandwidth of the processor and any which has an OI higher than the ridge point is limited by the processor's floating point capability.

The OI for the application studied in this paper is 0.54, which is lower than the ridge points of any of the architectures (given in Table \ref{tab:specsmatrix}), indicating that this simplified version of the application is memory bandwidth bound across the board (and thus not sensitive to floating point capability).
 This explains why the NVIDIA GTX780 performs so well despite the fact that it has very low floating point capability: its ridge point of 0.57 is still (just) higher than the application OI.

 The blue columns in Figure \ref{fig:AllBandwidths} give observed data throughput (volume of data loaded/stored by the application divided by runtime, assuming perfect caching), for each of the architectures.
 The black lines give the peak bandwidth capability of the hardware (as reported in Table \ref{tab:specsmatrix}).
 It can be seen that, for all but the Xeon Phi architecture, the measured throughput varies in line with the peak bandwidth, evidence that the overall performance of this application can largely be attributed to the memory bandwidth of that architecture.
 Since it is often very difficult to achieve near peak performance, we also include (with dashed horizontal lines) \textsc{stream} benchmark results (see Section \ref{sec:stream}), which give a more realistic measure of what is achievable.
 It can be seen that our results are, in general, achieving a reasonable percentage of \textsc{stream}
but we still have room for improvement.
In addition to running \textsc{stream}, profiling was also done on a selection of the runs.
These results showed higher values than our measured results, indicating that our assumption of perfect caching (see above) is not strictly true and there may be scope to reorganise our memory access patterns to improve the caching.
 The Xeon Phi is seen to achieve a noticeably lower percentage of peak bandwidth relative to the other architectures, and this warrants further investigation.

\subsection{Optimisation}
Two optimisation methods were explored for the GPU platforms: shared and texture memory. 
Texture memory is a read-only or write-only memory that is distinct from global memory  and uses separate caching allowing for quicker fetching for specific types of data (in particular, ones that take advantage of locality).
The use of texture memory is relatively straightforward in CUDA, requiring only an additional keyword for input parameters.
OpenCL is restricted to an earlier version on NVIDIA GPUs which does not support double precision in texture memory, so these results are not included.
Shared memory is specific to one of the lightweight ``cores'' of a GPU (streaming multi-processor on NVIDIA) and can  only can be shared between threads utilising that core, so can be useful for data re-use.
A 2.5D tiling method was used to take advantage of shared memory \cite{craigthesis}.
%Shared memory optimisations were explored using an XY-tiling method.
%This means that the Z index of a grid point is held constant while X and Y indices are calculated for a tile spanning the XY plane and then the Z index iterates.
%This algorithm was adopted from the same method proposed by Craig Webb\cite{craigthesis}.
%The  method was run using optimal threading configurations in CUDA, OpenCL and abstractCL for small and large rooms across two different platforms (one NVIDIA GPU and one AMD GPU).
%The CUDA version was also run using texture memory and all results were compared with codes with only thread optimisation. 
%

Figure \ref{fig:localMem} shows the results for this experiment, where the optimised versions are the fastest versions found in this project per version per platform.
Three different types of codes were run: \textit{sharedtex} uses shared and texture memory (for the CUDA version), \textit{shared} uses only shared memory and \textit{none} uses no memory optimisations.
As before, different platforms are indicated by separate segments of the graphs.
The reason for comparing to an optimised thread configuration version instead of the original run was to isolate what effect the memory optimisations had. 
Both room sizes (large and small) were run, but the large rooms had more significant differences in performance so are the only results shown.
Overall the abstractCL version showed the most consistent improvement, however in this graph it is more clear that it is not quite as fast as the OpenCL version due to the overhead of using a more productive framework. 
All versions showed some improvement with this use of shared memory, but this amount varied per version and per room size across the different architectures.
One of the reasons these results do not show more improvement is because the codes are already close to peak bandwidth as is discussed in Section \ref{sec:bandwidth}.
%The abstractCL version showed the most consistent improvement in performance: 15\% for small and large rooms on the AMD R9 259X2 chip and 4\% for the small and large rooms on the NVIDIA Kepler K20.
%For the OpenCL version,  the shared memory optimisation improved performance by $\sim$9\% for the small room and $\sim$15\% for the large room on the AMD R9 259X2.
%On the NVIDIA K20, this improvement was $\sim$6\% for the small room and $\sim$12\% for the large room.
%For the CUDA version on the K20, the performance increase was $\sim$7\% for the small room and $\sim$9\% for the large room  using texture and shared memory. 
%The performance increase is about half those values when using only shared memory.
%Another reason might be because there is not enough locality to exploit in a seven-point stencil.  
%
%%Results show that OpenCL-based codes show more improvement than the CUDA ones, whereby in large rooms there is an XX\% improvement for OpenCL whereas even including texture memory for CUDA versions, there is only a XX\$ improvement seen. 
%%(More explanation)
\begin{figure}[ht]
\center
\centerline{\includegraphics[scale=0.3]{images/use/localMemory512.pdf}}
\caption{\label{fig:localMem}{\it Memory optimisations for CUDA, OpenCL and abstractCL versions of the room acoustics benchmarks run for room size of 512$\times$512$\times$404 on an NVIDIA and AMD GPU. The timings shown produce 100ms of sound.}}
\end{figure}

\subsection{Advanced Simulations}
Results in this paper have thus far only been presented for a very simplified problem: wave propagation is assumed lossless and the simplest FDTD scheme (employing a seven-point stencil) is used. It is thus of interest to explore more complex models of wave propagation, as well as improved simulation designs. Two new features were investigated for these so-called  ``advanced'' codes: air viscosity (see \cite{craigthesis}) and larger stencil schemes of leggy type (see \cite{hamilton2015large}).
The main algorithmic difference in adding viscosity is that another grid is passed into the main kernel and more computations are performed.
Schemes operating over leggy stencils require access to grid values beyond the nearest neighbours.
For this investigation, 19-point leggy stencils were introduced (three points in each of the six Cartesian directions as well as the central point, see Figure 1 in \cite{hamilton2015large}). The comparison was done on a smaller scale, however, with the intention only of giving a general idea of whether or not the codes performed similarly. Thus, only CUDA and OpenCL versions were tested.
The variations were run on the two NVIDIA GPUs, the two AMD GPUs and the Xeon Phi for both small and large room sizes. 

Results of the performance of these advanced codes can be discussed in a number of ways including: in  comparison to the simpler codes, as a comparison amongst the different advanced versions,  as a comparison between versions on the same platform  and also comparisons of the same version across platforms.
Graphs in Figure \ref{fig:advTimings} show the performance timings and the memory bandwidth of the advanced codes for the various implementations for the larger room size. 
In these graphs, the following versions are included: cuda (the original version), cuda\_adv (cuda with viscosity), cuda\_leggy (cuda with leggy stencils) and  cuda\_leggyadv (cuda with leggy stencils and viscosity).
The comparable OpenCL counterparts of these versions were also run.
These graphs are set up in a similar way as those found in Figures \ref{fig:AllTimings} and \ref{fig:AllBandwidths}, where the versions of the code run along the x-axis and the performance  is on the y-axis (in seconds) and the separated parts of the graph indicate the platform it was run on.
Here, the top graph shows performance and the bottom shows bandwidth.

Generally the performance profile of the advanced codes looks similar to what can be seen for the original codes in Section \ref{sec:bestperformance}: the codes still run fastest on AMD R9 259X2 and slowest on the Xeon Phi. 
The  versions run on the AMD R9 259X2 in comparison to the same versions on the K20 hover between being 43\%-54\% faster. 
For both large and small rooms, the leggy codes are slower than the viscosity codes for both OpenCL and CUDA on everything except the K20.
The combination advanced codes (*\_leggyadv) are significantly slower across the board, particularly on the Xeon Phi. 

This purpose of this analysis is to see what effect algorithmic changes (ie. number of inputs or floating point operations) in the same benchmark have when run on the same platform.
How the performance changes with differences to the models of the rooms can vary quite a bit across platforms, which echoes the results seen when comparing local memory optimisations. 
When comparing original versions to the leggy, viscosity or combination versions, OpenCL codes are 1.4--6.4x slower for the combination versions. 
When this is limited to AMD GPUs, the difference is only 1.4x slower - for NVIDIA GPUs, 3--3.6x slower. 
In comparison, the CUDA version on the NVIDIA GPUs varies from 1.6--2.4x slower for the combination version. 
For the stand-alone leggy and viscosity versions, this difference is much less pronounced, however the same trend remains: OpenCL versions retain better performance with changes on AMD platforms and significantly worse than CUDA versions on NVIDIA GPUs. 
These differences cannot wholly be attributed to specification differences given that the difference exists for all these versions between the AMD R280 and NVIDIA GTX 780, which share some similar specifications.
\begin{figure}[ht]
\center
\centerline{\includegraphics[scale=0.3]{images/use/advancedTimings512.pdf}}
\centerline{\includegraphics[scale=0.3]{images/use/advancedBandwidth512.pdf}}
\caption{\label{fig:advTimings}{\it Timing (top) and Bandwidth (bottom) for different versions of the advanced room acoustics benchmarks across platforms for room size 512$\times$512$\times$404. Smaller room results are not included as they show similar pattern. }}
\end{figure}



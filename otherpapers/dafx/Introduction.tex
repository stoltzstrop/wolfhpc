\section{Introduction}
\label{sec:intro}

The finite difference time domain method (FDTD) is a well-known numerical
approach for acoustic modelling of the 3D wave equation \cite{botteldooren}.
 Space is discretised into a three-dimensional grid of points, with data values resident at
each point representing the acoustic field at that point.
 The state of the system evolves through time-stepping: the value at each point is repeatedly updated using {\it finite differences} of that point in time and space.
 The so-called {\it stencil} of points involved in each update is determined by the choice of discretisation scheme for the partial differential operators in the wave equation \cite{bilbao2013large}.  
This numerical approach is relatively computationally expensive, but amenable to
parallelisation.
 In recent years, there has been good progress in the development of techniques to exploit modern parallel hardware. 
 In particular, NVIDIA GPUs have proven to be a very powerful platform for acoustic modelling \cite{hamilton2015large}\cite{rober2006waveguide}. 
Most work in this area has involved writing code using the NVIDIA specific CUDA language, which is tied to this platform.
 Ideally, however, any software should be able to run in a portable manner across different architectures, such that the performance of alternatives can be explored, and different resources can be exploited both as and when they become available.
 It is non-trivial, however, to develop portable application source code that can perform well across different architectures: this issue of {\it performance portability} is currently of great interest in the general field of High Performance Computing (HPC) \cite{hpcwire}.

In this paper, we explore the performance portability issue for FDTD numerical modelling of the 3D wave equation.
 To enable this study, we have developed different implementations of the simulation, each
performing the same task, including a CUDA implementation that acts as a ``baseline'' for use on NVIDIA GPUs, plus other more portable alternatives.
 This enables us to assess performance across multiple different hardware solutions: NVIDIA GPUs, AMD GPUs, Intel Xeon Phi many-core CPUs and traditional multi-core CPUs.
 The work also includes the development of a simple, adjustable abstraction framework, where the flexibility comes through the use of templates and macros (for outlining and  substituting code fragments) to facilitate different implementation and optimisation choices for a room acoustics simulation.
Both basic and advanced versions of an FDTD algorithm that simulates sound propagation in a room (i.e. a cuboid) are explored.


 This paper is structured as follows: in Section \ref{sec:background}, we give necessary background information on the computational scheme, the hardware architectures under study and the associated programming models; in Section \ref{sec:frameworks}, we describe the development of a performant, portable and productive room acoustics simulation framework; in Section \ref{sec:expsetup} we outline the experimental setup for  investigating different programming approaches and assessing performance; in Section \ref{sec:results} we present and analyse performance results; and finally we summarise and discuss future work in Section \ref{sec:summary}.


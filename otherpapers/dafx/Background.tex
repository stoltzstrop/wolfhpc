\section{Background}
\label{sec:background}

This paper is focused on assessing different software implementations of a room acoustics simulation across different types of computing hardware.
While this project focuses strictly on single node development, the ideas in this work could easily be extended for use across multi-node platforms by coupling with a message-passing framework.
 In this section we give some necessary background details.
 We first describe the FDTD scheme used in this study. 
 We then give details on the hardware architectures that we wish to assess and the native programming methods for these architectures. 
 Finally we describe some pre-existing parallel programming frameworks that provide portability.

 
\subsection{FDTD Room Acoustics Scheme}
\label{sec:algorithm}

Wave-based numerical simulation techniques, such as FDTD, are concerned with deriving algorithms for the numerical simulation of the 3D wave equation:
\begin{equation}
\frac{\partial^2 \Psi}{\partial t^2} = c^2\nabla^2 \Psi
\end{equation}
Here, $\Psi\left({\bf x},t\right)$ is the dependent variable to be solved for (representing an acoustic velocity potential, from which pressure and velocity may be derived), as a function of spatial coordinate ${\bf x}\in{\mathcal D}\in{\mathbb R}^{3}$ and time $t\in{\mathbb R}^{+}$. 

In standard finite difference time domain (FDTD) constructions, the solution $\Psi$ is approximated by a grid function $\Psi_{l,m,p}^{n}$, representing an approximation to $\Psi\left({\bf x} = \left(lh,mh,ph\right),t=nk\right)$, where $l,m,p,n$ are integers, $h$ is a grid spacing in metres, and $k$ is a time step in seconds. The FDTD scheme used for this work is given in \cite{craigthesis}, and is the standard scheme with a seven-point Laplacian stencil. According to this scheme, updates are calculated as follows:

\begin{equation}
\Psi^{n+1}_{l,m,p}=(2-6\lambda^2)\Psi^{n}_{l,m,p}+\lambda^2S-\Psi^{n-1}_{l,m,p}
\label{eq:room}
\end{equation}
where
\begin{equation}
\begin{split}
S=\Psi^{n}_{l+1,m,p}+\Psi^{n}_{l-1,m,p}+\Psi^{n}_{l,m+1,p}\\+\Psi^{n}_{l,m-1,p}+\Psi^{n}_{l,m,p+1}+\Psi^{n}_{l,m,p-1},
\end{split}
\end{equation}
 The constant $\lambda = ck/h$ is referred to as the Courant number and must satisfy the stability condition $\lambda\leq 1/\sqrt{3}$.
 It can be seen that each grid value is updated based on a combination of two previous values at the same location, and contributions from each of the six neighbouring points in three dimensions (giving
the six terms in $S$ - see Figure \ref{fig:stencil}).
\begin{figure}[ht]
\center
\centerline{\includegraphics[scale=0.3]{images/use/stencil.pdf}}
\caption{\label{fig:stencil}{\it Figure of a 7-point stencil on a three dimensional grid.}}
\end{figure}
The benchmarks used in this study are run over 13--105 million grid points at a sample rate of 44.1kHz to produce 100ms of sound.
 % SB: at what sample rate? Corresponding to approximately what room volumes?
 The boundary conditions used are frequency-independent impedance boundary conditions.
 We also include, towards the end of the paper, some results for comparison where a larger stencil
 is used, as described in \cite{hamilton2015large}\cite{largestencil}.
\subsection{Hardware and Native Programming Methods}
\par
% micro history of computing architecture
The hardware architectures used in this work are multi-core CPU, GPU, and many-core CPU (Intel Xeon Phi) platforms.
  CPUs have traditionally been the main workhorses for scientific computing and are still
targeted for the majority of scientific applications.
 GPUs have been gaining traction in the scientific computing landscape over the last
decade, and can offer significant performance improvements over traditional CPUs for certain algorithms including wave-based ones like acoustic simulation.
 Xeon Phi chips are essentially multi-core CPUs, but with a large number of cores each
with lower clock speeds and wider vector units.
 In this section, we describe these architectures in more detail and discuss how they are
typically programmed.


\subsubsection{Traditional Multi-Core CPUs}

Computational simulations (like acoustic models) have historically been run on CPU chips.
However, these architectures were originally optimised for sequential codes, whereas scientific applications are typically highly parallel in nature.
Since the early 2000s, clock speeds of CPUs have stopped increasing due to physical constraints. 
Instead, multi-core chips have dominated the market meaning CPUs are now parallel architectures.


OpenMP \cite{openmp} is one framework designed for running on shared memory platforms like CPUs.
 It is a multi-threading parallel model which uses compiler directives to determine how an algorithm is split and assigned to different threads, where each thread can utilise one of the multiple available cores.
 OpenMP emphasises ease of use over control, however there are settings to determine how data or algorithmic splits occur.


\subsubsection{GPUs}
\label{sec:GPUs}
% GPU overview
% TODO: MAKE SURE TERMINOLOGY IS CONSISTENT !! (ie. either CUDA or OpenCL - probably CUDA)
GPUs were originally designed for accelerating computations for graphics, but have increasingly been re-purposed to run other types of codes \cite{massivelyparallel}.
As such, the architecture of GPUs has evolved to be markedly different from CPUs.
Instead of having a few cores, they have hundreds or thousands of lightweight cores that operate in a data-parallel manner and can thus do many more operations per second than a traditional CPU.
However most scientific applications, including those in this paper, are more sensitive to memory bandwidth: the rate at which data can be loaded and stored from memory.
 GPUs offer significantly higher memory bandwidths over traditional CPUs because they use graphics memory, though they are not used in isolation but as ``accelerators'' in conjunction with ``host'' CPUs.
Programming a GPU is more complicated than a CPU as the programmer is responsible for offloading computation to the GPU with a specific parallel decomposition as well as managing the distinct memory spaces.

In this paper we investigate acoustic simulation algorithms using NVIDIA and AMD GPUs.
 NVIDIA GPUs are most commonly programmed using the vendor-specific CUDA model \cite{cuda}, which extends C, C++ or Fortran.
 CUDA provides functionality to decompose a problem into multiple ``blocks'' each with multiple ``CUDA threads'', with this hierarchical abstraction designed to map efficiently onto the hardware which correspondingly comprises multiple ``Streaming Multiprocessors'' each containing multiple ``CUDA cores''.
 CUDA also provides a comprehensive API to allow memory management on the
distinct CPU and GPU memory spaces.
 CUDA is very powerful, but low level and non-portable.
AMD GPUs are similar in nature to NVIDIA GPUs, but since there is no equivalent vendor-specific AMD programming model, the most common programming method is to use the cross-platform OpenCL, which we discuss in  Section \ref{sec:opencl}.
For simplistic purposes, we will use the same terminology to describe the OpenCL framework as for CUDA.


\subsubsection{Intel Xeon Phi Many-core CPU}
%overview of Xeon Phi

The Xeon Phi was developed by Intel as a high performance many-core CPU for scientific computing \cite{xeonphi}. 
One of the main benefits of the Xeon Phi is that it uses the same instruction set (X86) as the majority of other mainstream CPUs.  
This means that, theoretically, codes developed to run on CPUs could be more easily ported to Xeon Phi chips.
There are fewer cores on the Xeon Phi than on a GPU, however data does not need to be transferred to and from separate memory.
There is also a wider vector instruction set on the Xeon Phi, which means that more instructions can be run in parallel per core than on a CPU or GPU. 
Depending on the algorithm, this can provide a boost in performance.
The Xeon Phi currently straddles both the architecture and the performance of the CPU and GPU.  
The same languages and frameworks that are used for programming CPUs can be used on Xeon Phis.


\subsection{Existing Portable Programming Methods}

In this section we review  existing portable parallel frameworks and APIs: OpenCL, a low-level API designed for use on heterogeneous platforms; TargetDP, a lightweight framework that abstracts data-parallel execution and memory management syntax in a performance portable manner, and a range of other frameworks offering higher levels of abstraction and programmability. 
These frameworks and APIs are designed to allow computational codes (like room acoustics) to be portable across different architectures, however they can be difficult to program in and they do not all account for performance across hardware.

\subsubsection{OpenCL}
\label{sec:opencl}
OpenCL \cite{opencl} is a cross-platform API designed for programming heterogeneous systems.
 It is similar in nature to CUDA, albeit with differing syntax.
 Whereas CUDA acts as a language extension as well as an API, OpenCL only acts as the latter resulting in the need for more boilerplate code and provides a more low-level programming experience.
 OpenCL can, however, be used as a portable alternative to CUDA, as it can be executed on NVIDIA, AMD and other types of GPUs, as well as manycore CPUs such as the Intel Xeon Phi.
 OpenCL is compatible with the C and C++ programming languages.

\subsubsection{targetDP}

The targetDP programming model \cite{gray2016lightweight} is designed to target data-parallel hardware in a platform agnostic manner, by abstracting the hierarchy of hardware parallelism and memory systems in a way which can map on to either GPUs or multi/many-core CPUs (including the Intel Xeon Phi) in a platform agnostic manner.
  At the application level, targetDP syntax augments the base language (currently C/C++), and this is mapped to either CUDA or OpenMP threads (plus vectorisation in the latter case) depending on which implementation is used to build the code.
 The mechanism used is a combination of C-preprocessor macros and libraries.
 As described in \cite{gray2016lightweight}, the model was originally developed in tandem with the complex fluid simulation package \textit{Ludwig}, where it exploits parallelism resulting from the structured grid-based approach.
 The lightweight design facilitates integration into complex legacy applications, but the resulting code remains somewhat low-level so it lacks productivity and programmability. 

\subsubsection{Other Methods}
Higher level approaches focus more on distinct layers of abstraction that are far removed from the original codes. 
These approaches include: parallel algorithmic skeletons, code generators, DSLs (Domain Specific Languages), autotuners, combinations thereof and others.
Higher-level frameworks can also provide  decoupling layers of functionality, which allows for more flexibility with different implementations and architectures.  
As many of these frameworks are still in early stages of development, there are limitations in using them with pre-existing code bases.
Many of these higher-level frameworks build on the work of skeleton frameworks, which focus on the idea that many parallel algorithms can be broken down into pre-defined building blocks \cite{murraybook}.
Thus, an existing code could be embedded into a skeleton framework that already has an abstraction and API built for that algorithm type, such as the stencils found in room acoustics models.
These frameworks then simplify the process of writing complex parallel code by providing an interface which masks the low-level syntax and boilerplate.
A code generator can either be a type of compiler or more of a source to source language translator. 
Other higher-level approaches include functional DSLs with auto-tuning \cite{zhang_DSL}, rewrite rules \cite{spiral}, skeleton frameworks combined with auto-tuning \cite{skeleton1} and many others including the examples below.
Liquid Metal is a project started at IBM to develop a new programming language purpose built to tailor to heterogeneous architectures \cite{liquidmetal}.
Exastencils is a DSL developed by a group at the University of Passau that aims to create a layered framework that uses domain specific optimisations to build performant portable stencil applications \cite{exastencil}.

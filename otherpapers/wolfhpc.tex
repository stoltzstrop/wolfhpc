
\begin{abstract}

The parallel programming landscape has become so vast and complex that developing simulation codes which are performant, portable and easily programmable requires compromising somewhere. 
Existing approaches have focused on abstracting away parallelisation instructions from algorithms, using libraries to hide details of hardware specific code and low-level syntax. 
However, no current solutions provide all three of performance, portability and productivity.
In addition, many have been tested only on simple benchmarks or focus on specific hardware optimisations, resulting in a lack of performance portability.
This project proposes developing a modular solution for  providing performance, portability and productivity from the top down, beginning with algorithms of interest: 3D wave-based simulations discretised to stencils.
By employing the so-called ``assembly language of HPC'' \textsc{Lift}\cite{lift}\cite{liftcgo}, we propose to develop and enhance this intermediary language to enable computation and optimisation for complex 3D stencils, in particular those with absorbing boundary conditions as found in wave simulations.
We then propose to harness the abstractions of existing stencil DSLs to create a modularised methodology for wave-based simulation development. 
This will involve adapting a top-level DSL to compile into \textsc{Lift}, which can then use \textsc{Lift}'s composability and rewrite rules to compile these models into performant, portable and  productive code.


\end{abstract}


\section{Motivation}
Many 3D wave-based simulations - such as room acoustics\cite{craigthesis} or ground penetrating radar\cite{gprmax} -  are complicated to model and even more complex to abstract out.
However, such simulations are integral to predicting the properties and behaviour of the physical world around us. 
%Previous research has shown that it is possible to develop more productive and performance portable codes for simulation codes (such as room acoustics)  which simplify the problems of writing programmable and performant code across different platforms (ref to masters?).
A large number of high-level parallel methodologies currently exist which focus on stencil applications, however they  neglect to manage absorbing boundary conditions, which are critical to obtaining accurate results for 3D wave models.
Instead of writing yet another stencil library or DSL to address this shortcoming, this project aims to take advantage of what is available already and create a modularised approach to simulation development for HPC using existing DSLs and the \textsc{Lift} intermediary language framework.

Computational scientists should be able to focus on their own research and not require HPC expertise.
As the HPC landscape expands, developers require the ability to run their codes on newer, more performant platforms - such as GPUs or Xeon Phis - however, there are a lack of tools that provide performance portability. 
Instead, programmers are required to rewrite and retune their codes as well as maintain multiple code bases.
This process can be expensive, time-consuming and prone to error.
This project will approach these issues with a  modularised solution to the development of performant, portable and productive 3D wave models using existing frameworks.


\section{Background}
\subsection{3D Wave Simulations}
% Physical simulations and Stencils 
3D wave-based simulations are an important tool in physics for modeling the evolution of waves through space and time of various mediums. 
The finite difference time domain method (FDTD) is a widely used numerical approach for modelling of the 3D wave equation\cite{botteldooren}, which is shown in Equation \ref{fig:3Dwave}.

\begin{equation}
\frac{\partial^2 \Psi}{\partial t^2} = c^2\nabla^2 \Psi
\label{fig:3Dwave}
\end{equation}

Space is discritised into a three-dimensional grid of points, with data values resident at each point representing the field at that point.
The state of the system evolves through time-stepping: values at each point are repeatedly updated using ``finite differences'' of values in the neighbourhood of that point.
This algorithm, known as a stencil, updates points as determined by the choice of discretisation scheme for the partial differential operators in the wave equation \cite{bilbao2013large}.  
This approach can be computationally expensive, however parallelisation techniques have shown great improvements in performance.
Though there has been progress in the development of techniques to exploit modern parallel hardware, much of it is low-level or tied to specific platforms. 
Ideally, scientific models should be able to run in a portable manner across different architectures while retaining performance and being straightforward to program.

 % figure of stencil?
 % figure of 3D wave equation ?

 \subsection{Related Work}

%Current solutions
There are currently a wide range of DSLs, code generators, skeleton libraries and other high-level approaches that focus on stencil applications (ie. Halide\cite{halide}, Pochoir\cite{pochoir}, Exastencils\cite{exastencils} and so on).
However, these solutions are limited in the types of stencils they focus on and do not offer performance portability.
Additionally, because all of these frameworks focus on stencil applications, similar functionality for common requirements has been developed in many of them.
Ideally, DSLs could use a common compiler, so that the writers of these languages could focus on the abstraction layer at the top instead of having to manage hardware-specific low-level optimisations. 
This is where the \textsc{lift} language provides great utility.
\textsc{lift} is designed to be used as an intermediate layer targeted by DSLs to handle low-level implementation and optimisation.
The language is built using a composition of primitives for a wide range of algorithms (including stencils).
Figure \ref{fig:primitives} shows a sampling of some of these primitives and how they work.
A search space of ``rewrite rules'' (ie. rules that describe valid transformations of specific primitive compositions) is then used to optimise codes for a particular platform.
Kernels are then produced in OpenCL for all the different ``rewrites'' until an optimal program is found. 
Currently, the only backend \textsc{lift} supports is OpenCL, however the modular design of the language makes it easily extensible to other parallel programming frameworks.

\begin{figure}
  \centering
 \resizebox{.7\hsize}{!}{\includegraphics[width=17cm]{algorithmicprimitivescut.pdf}}
 \caption{Algorithmic Primitives in the \textsc{Lift} Language}
  \label{fig:primitives}
\end{figure}


\section{3D Wave Simulation Development in the \textsc{Lift} Framework}
%What has been done 
\subsection{Current Work}
Preliminary results have shown that \textsc{Lift} is capable of expressing stencils of varying types and sizes.
In particular, simplified room acoustics simulations have been thoroughly investigated in the framework and a number of other 2D and 3D benchmarks have also been implemented. 
However, the performance of 3D stencils lags behind hand-optimised versions, so current work also involves developing and formalising stencil optimisations for 3D models, in particular 2.5D tiling.
Additionally, ground penetrating radar algorithms are also being implemented in the framework as they have similar wave behaviour to room acoustics. 
These are also modelled using the 3D wave equation and have absorbing boundary conditions, but involve both the electric and magnetic fields interacting with each other.
\subsection{Future Work}
So far only simplified versions of 3D wave models have been implemented in \textsc{lift}. 
Thus, how to best abstract out absorbing boundary conditions needs to be investigated in more detail and primitives to accommodate these conditions need to be designed and added.
Additionally, a stencil-based DSL needs to be extended to compile into the \textsc{Lift} language for use as a front end.
As well as these larger contributions, smaller issues such as targeting multiple cores or GPUs and iterative stencils need to be explored further. 

\section{Conclusion}
This work aims to provide a modular, reusable methodology for developing performance portable and easily programmable physical simulation models, in particular 3D wave models with absorbing boundary conditions.
Developing this will involve extending the \textsc{lift} framework to accommodate complex 3D stencils and harnessing a DSL to compile into the \textsc{lift} language.
Following suit, other types of DSLs and algorithms can use a similar approach.
This will lead to opportunities for performance portable and programmable code for scientific  models targeting HPC systems.
%


%Make sure that your PDF file does not contain page numbers, is
%up to 4 pages long and is formatted for an A4 page.
%Upload the final version before June 8 on the website.

